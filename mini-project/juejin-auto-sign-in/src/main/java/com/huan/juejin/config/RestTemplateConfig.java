package com.huan.juejin.config;

import com.huan.juejin.properties.JuejinProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

/**
 * @author huan.fu 2021/11/18 - 下午5:20
 */
@Configuration
public class RestTemplateConfig {
    
    @Autowired
    private JuejinProperties juejinProperties;
    
    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder
                .setConnectTimeout(Duration.ofSeconds(3))
                .setReadTimeout(Duration.ofSeconds(6))
                .additionalInterceptors((request, body, execution) -> {
                    request.getHeaders().add("cookie", juejinProperties.getCookie());
                    return execution.execute(request, body);
                })
                .build();
    }
}
