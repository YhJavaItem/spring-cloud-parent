package com.huan.juejin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 启动类
 *
 * @author huan.fu 2021/11/18 - 下午5:09
 */
@SpringBootApplication
@EnableScheduling
public class JuejinApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(JuejinApplication.class);
    }
}
