package com.huan.study.excel.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 学生类
 *
 * @author huan.fu
 * @date 2023/3/21 - 20:37
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Student {
    /**
     * 班级名
     */
    private String className;
    /**
     * 课程名
     */
    private String courseName;

    /**
     * 学生名
     */
    private String studentName;
    /**
     * 年龄
     */
    private int age;

}
