# spring-cloud-parent

#### 项目介绍

spring cloud 的一个简单的学习

| 项目名                       | 描述                                                 |
|---------------------------|----------------------------------------------------|
| eureka-server             | eureka-server的单机服务端，增加security保护服务端,关闭自我保护，具体操作见日志 |
| eureka-client             | eureka-client的单机客户端                                |
| eureka-server-ha          | 服务端的高可用配置                                          |
| ribbon                    | ribbon的简单使用                                        |
| feign                     | feign的简单使用                                         |
| feign-conf                | feign的各种配置的使用                                      |
| hystrix                   | 和hystrix相关的使用                                      |
| hystrix-dashboard-turbine | hystrix的dashboard和turbine                          |
| zuul                      | 服务网关的使用                                            |
| config                    | 配置中心的使用                                            |
| monitor                   | 服务监控                                               |
| prometheus                | 指标埋点                                               |
| springboot                | 处理SpringBoot中的一些特性                                 |
| es                        | 处理es相关的内容                                          |
| mybatis                   | 处理mybatis相关的内容                                     |
| sharding-jdbc             | 处理分表的逻辑                                            |
| canal                     | canal相关的内容                                         |
| security                  | security安全相关                                       |
| seata                     | 分布式事物seata的相关用法                                    |
| redis                     | redis的一些用法                                         |
| mini-project              | 一些小的测试项目                                           |
| drools                    | drools规则引擎的一些用法                                    |
| cola                      | COLA应用架构的简单使用                                      |
| spring-batch              | Spring Batch的一些简单的用法                               |
| hadoop                    | hadoop相关的一些代码                                      |
| flink                     | flink项目相关的一些代码                                     |

~~~
spring-cloud-parent
│ |- 父项目
│ >> eureka优雅停机[博客](https://blog.csdn.net/fu_huo_1993/article/details/115255222?spm=1001.2014.3001.5501)
├─eureka-server [博客](http://huan1993.iteye.com/blog/2423631)
│ |- 项目中用到的eureka server 注册中心
│ │     >> 开启注册中心的权限验证
│ │     >> 关闭eureka 的自我保护（开发环境修改，正式环境不要修改）
│ │     >> 修改清除节点的时间（开发环境修改，正式环境不要修改）
│ │     >> 修改instanceId的值
│ │     >> 显示ip地址，而不是使用主机名
├─eureka-client
│ |- 一个单独的服务提供者模版
│ │     >> 如何连接到有权限的服务段
│ │     >> 减小客户端和服务端的心跳和租约（开发环境修改，正式环境不要修改）
├─eureka-server-ha [博客](http://huan1993.iteye.com/blog/2423635)
│  ├─eureka-server-ha-8764
│  │ |- 8764注册中心   
│  ├─eureka-server-ha-8765
│  │ |- 8765注册中心 
│  ├─eureka-client-8766
│  │    >> 演示如何注册到高可用的注册中心上 
│  │    >> 本地测试需要配置虚拟域名
│  │    >> 本地测试需要注意prefer-ip-address的值，看eureka server界面上显示是否正确，即有没有显示当前节点的复制节点
├─ribbon [博客](http://huan1993.iteye.com/blog/2423850)
│  ├─product-provider-8777
│  ├─product-provider-8778
│  ├─product-provider-8779
│  ├─product-provider-8780
│  ├─order-consumer
│  │    >> 为具体的某个服务单独配置策略，比如修改负载均衡策略等
│  │    >> 需要注意全局和局部配置的区别
│  │    >> 使用配置文件进行配置(略)
│  │    >> 解决ribbon项目第一次访问时才加载 ribbon.eager-load.enabled:true  ribbon.eager-load.clents: 需要立即启动client的名字
├─feign [博客](http://huan1993.iteye.com/blog/2423924)
│  ├─product-provider-8083
│  ├─product-provider-8084
│  ├─product-consumer-8082
│  │    >> 演示 feign 的调用远程服务的各种参数的调用形式
│  │    >> 演示 feign 调用方法参数的各种坑
├─feign-conf [博客](http://huan1993.iteye.com/blog/2424108)
│  ├─product-provider-8085
│  ├─product-provider-8086
│  ├─product-provider-8087
│  ├─product-provider-8088
│  ├─product-consumer-8089
│  │    >> feign 的默认配置类 (FeignClientsConfiguration)
│  │    >> 单独对某个客户端修改配置
│  │    >> 修改默认的契约
│  │    >> 修改feign的日志级别
│  │    >> 根据url直接进行调用，和增加自定义请求头  （如果使用了hystrix,那么在RequestInterceptor中要想使用ThreadLocal的值，隔离策略需要修改成信号量隔离）
│  │    >> 增加压缩
│  │    >> 增加超时时间的配置
│  │    >> 配置重试
│  │    >> yml 文件中对feign的各种配置
│  │
├─hystrix [回退](http://huan1993.iteye.com/blog/2424282)
│  ├─product-provider-8091
│  ├─product-consumer-feign-hystrix-8093
│  │    >> feign 中使用 fallback 实现回退
│  │    >> feign 中使用 fallbackFactory 实现回退，拿到回退的原因
│  │    >> 修改feign的日志级别
│  ├─product-consumer-feign-hystrix-isolation-dashboard-8094
│  │    >> 使用 hystrix 的 dashboard 监控单个微服务的各种参数
│  │    >> 配置全局的隔离策略
│  │    >> 配置某个具体方法的隔离策略
│  │    >> 修改HystrixCommad命令的超时时间
│  │    >> 修改回退的最大并发
│  │    >> 修改局部或全局的Thread隔离时最大的线程并发数
│  │    >> 修改 fallback 最大的并发数
│  │    >> 修改 feign 中默认使用 HttpUrlConnection 进行远程方法请求，修改成 apache httpclient
│  │        ** 需要增加 feign-httpclient 依赖，其余默认可以不用修改，也可以修改一些httpclient的参数
│  │  >> 请求缓存（略)
│  │  >> 请求合并(略) => 当某个请求在极短的时间内大量请求可以使用请求合并来提高效率
├─hystrix-dashboard-turbine [hystrix图表监控](http://huan1993.iteye.com/blog/2424491)
│  ├─hystrix-dashboard
│  │   >> 监控单个工程（指的时同一个服务消费者部署一个，监控这一个的数据）
│  ├─hystrix-single-cluster-turbine
│  │   >> 监控单个集群（指的是同一个服务消费者部署多个，监控这一整个的数据）
│  ├─hystrix-more-cluster-turbine
│  │   >> 监控多个集群（指的是存在多个服务消费者，且多个服务消费者可能都部署了多个，监控这多个的数据）    
│  ├─hystrix-all-cluster-turbine
│  │   >> 监控默认的集群
├─zuul [zuul的各种配置](http://huan1993.iteye.com/blog/2424491)
│  ├─product-provider-8202
│  ├─product-provider-8203
│  │   >> 服务提供者
│  ├─product-consumer-8201
│  │   >> 服务消费者
│  ├─product-gateway-8204
│  │   >> 网关程序
│  │   >> 查看 zuul 中配置好的路由和过滤器信息
│  │   >> 忽略所有微服务或某些微服务
│  │   >> 忽略所有为服务，只路由指定的微服务
│  │   >> 通过path和url访问到具体的某台机器上
│  │   >> 脱离eureka进行访问，并使之具有负载均衡和隔离的机制等
│  │   >> 转发前是否去掉路由前缀
│  │   >> 为所有路由都增加一个通过的前缀
│  │   >> 忽略某些路径不进行路由
│  │   >> 敏感头的传递（比如Cookie等）全局设置和某个微服务设置
│  │   >> 忽略头
│  │   >> spring security 在classpath 下会忽略的头
│  │   >> 本地调换和路由的优先级
│  │   >> 网关(zuul)的超时配置，看网关中yml中的配置
│  │   >> 重写 Location 头
│  │   >> 文件上传处理
│  │   >> zuul 中使用ribbon进行负载均衡调用，ribbon是在第一次调用时由Spring Cloud延时加载，现在修改成程序启动时就立即加载
│  ├─product-gateway-filters-8205
│  │   >> 使用 pre 过滤器进行权限的校验
│  │   >> 使用 post 过滤器添加额外的响应头
│  │   >> 禁用过滤器
│  ├─product-gateway-fallbackprovider-8206
│  │   >> zuul 服务网关的回退，即zuul访问某个微服务访问不到时的 fallback 处理
│  │   >> 单个服务回退 getRoute() 直接返回那个服务的serviceId的值，为所有的回退，直接返回 null 或 *
│  ├─product-gateway-aggregation-swagger2-8207
│  │   >> 聚合各个工程的 swagger api 接口文档
├─config [spring cloud config 的使用](http://huan1993.iteye.com/blog/2425032)
│  ├─config-server-8301
│  │   >> config server 端的编写并注册到eureka上
│  │   >> config server 端增加 basic 认证
│  │   >> config server 配置路径查找规则
│  │   >> config server 的加密和解密端点
│  │   >> 配置进行加密处理
│  │   >> 其它的一些配置见具体的配置文件上
│  ├─product-provider-config-client-8302
│  │   >> config client 端
│  ├─config-bus-webhook [spring cloud config 结合 spring cloud bus实现配置自定的刷新](http://huan1993.iteye.com/blog/2425170)
│  ├────config-server-bus-8305
│  │       >> config server 服务端，整合spring cloud bus 实现配置自动刷新
│  ├────product-provider-config-client-bus-webhook-8303
│  ├────product-provider-config-client-bus-webhook-8304
│  │       >> config client 客户端，git 的webhook请求config server的 /bus/refresh完成各个客户端配置的自动刷新
├─monitor 
│  ├─admin [spring boot admin 的使用](http://huan1993.iteye.com/blog/2425265)
│  │   >> 使用spring boot admin 进行监控
│  │────spring-boot-admin-server-8401
│  │      >> 在 spring boot 1.5.x 后，要监控端点，需要management.security.enabled=false
│  │      >> 引入 spring security 后，就可以使用 spring security 保护admin server
│  │      >> 引入 spring security 保护admin server
│  │      >> 作为一个服务发现者注册到 eureka 上
│  │      >> 给 admin server 提供一个表单登录
│  │      >> 服务的下上线进行邮件的通知
│  │────client-product-provider-8402
│  │      >> 动态控制日志级别
│  │      >> 当设置了management.context-path，如何进行配置
│  │      >> 显示版本号
│  │      >> 客户端使用了 spring security 做了权限保护，admin server如何访问
│  │      >> 整合 hystrix ui 
│  ├─zipkin 
│  │   >> 使用 zipkin 进行分布式服务追踪，分析服务调用耗时、服务之间的依赖
│  │────product-consumer-feign-hystrix-zipkin-8502
│  │────product-provider-zipkin-8501
│  │────zipkin-server-8503
│  │      >> 以上三个应用程序为一组，实现 zipkin 数据存储在 mysql 中
│  │      >> 如何进行配置参考 spring-cloud-parent -- monitor -- readme.txt 文件
├─prometheus 
│  ├─actuator-prometheus [博客 springboot接入prometheus](https://blog.csdn.net/fu_huo_1993/article/details/114841836)
│  │   >> 接入Prometheus
│  │   >> 自定义业务指标埋点
│  │   >> 增加全局tag，过滤tag，个性化MeterRegistry
│  ├─eureka-prometheus-parent
│  │   >> 使用 eureka 作为 prometheus 的服务发现
│  │   >> 在1分钟之内支付订单支付失败产生告警数据
│  │      >> 具体参考 eureka-prometheus-parent/order-provider-10004/订单支付失败告警.md文件
│  ├─prometheus-histogram
│  │   >> 在prometheus中使用直方图
├─springboot 
│  ├─bean-definition-registrar [博客](https://blog.csdn.net/fu_huo_1993/article/details/115699479?spm=1001.2014.3001.5501)
│  │   >> 实现手动动态的注入Bean。
│  │   >> 实现 ImportBeanDefinitionRegistrar 接口，实现手动注入 BeanDefinition
│  │      >> 1、实现此接口的类需要配合@Configuration和@Import注解使用。
│  │   >> 实现 FactoryBean 接口，实现个性化构建Bean。
│  │      >> 1、getObject() 方法返回的对象的内部是无法使用@Autowired等注解注入的，使用自己使用applicationContext.getAutowireCapableBeanFactory().autowireBean(instance)实现等等。
│  ├─logstash-grok [博客](https://blog.csdn.net/fu_huo_1993/article/details/116744920?spm=1001.2014.3001.5501)
│  │   >> 实现使用 logstash 的 grok 插件解析springboot程序的日志
│  │   >> 实现自定义 grok 的模式。
│  │   >> 实现多行匹配
│  ├─spring-parent-child-context [博客](https://blog.csdn.net/fu_huo_1993/article/details/117708365?spm=1001.2014.3001.5501)
│  │   >> 实现Spring的父子上下文
│  ├─class-method-pointcut
│  │   >> 使用 Pointcut 和 Advisor 实现切面效果。（类似于@Validate的实现效果）
│  ├─springboot-task
│  │   >> 使用 @Scheduled 注解实现定时任务
│  │   >> 程序启动时，动态注册定时任务
│  │   >> 使用 ThreadPoolTaskScheduler 实现动态的注册定时任务，需要修改ThreadPoolTaskScheduler的线程数，默认值是1
│  │   >> 验证 cron 表达式的正确性和计算下次执行的时间
│  │        >> Spring 的低版本使用 CronSequenceGenerator
│  │        >> Spring 的高版本使用 CronExpression
│  ├─spring-deferred-result [异步处理](https://blog.csdn.net/fu_huo_1993/article/details/120780925)
│  │   >> 实现异步处理
│  │   >> 可以实现 长轮训 操作
│  ├─springboot-encrypt-config [博客](https://blog.csdn.net/fu_huo_1993/article/details/120826254)
│  │   >> 实现配置属性的加密
│  ├─springboot-plugin 使用场景：见项目下的README.md文件
│  │   >> Spring Plugin 工程的使用
│  ├─springboot-redis-lua [博客](https://blog.csdn.net/fu_huo_1993/article/details/121001911)
│  │   >> springboot中在redis中调用lua脚本
│  │   >> 使用 lua 实现一个简单的锁
│  │   >> 使用 lua 实现一个简单分布式限流
│  ├─springboot-method-argument-resolver [博客](https://blog.csdn.net/fu_huo_1993/article/details/121774150)
│  │   >> 自定义参数解析器(类似于实现@RequestParam)
│  ├─springboot-override-exists-bean [博客](https://blog.csdn.net/fu_huo_1993/article/details/124313557)
│  │   >> 替换掉别人定义的@Bean或@Component实现，BeanDefinitionRegistryPostProcessor的使用
│  ├─springboot-extension-point(SpringBoot扩展点-见具体项目下的ReadME文件)
│  ├───failureanalyzer
│  │        >> 将程序在启动阶段发生的异常翻译成可读形式
│  ├───environmentpostprocessor [博客](https://blog.csdn.net/fu_huo_1993/article/details/124396988)
│  │        >> EnvironmentPostProcessor 该扩展点是在 ApplicationContext 刷新之前执行，可以让我们自定义环境（Environment）中的值
│  │        >> 此处我们完成我们自定义的属性覆盖掉默认配置文件中的属性
│  │        >> 日志无法输出的解决办法
│  │        >> org.springframework.boot.context.logging.LoggingApplicationListener 初始化日志系统
│  │        >> 可以实现配置的加解密，此处没有实现
│  ├───autoconfiguration
│  │        >> 自动配置的类需要加载@Configuration注解
│  │        >> 在spring.factories中配置org.springframework.boot.autoconfigure.EnableAutoConfiguration=自动配置的类
│  ├───beandefinitionregistrypostprocessor
│  │        >> 系统中的BeanDefinition都加载完了，但是没有进行实例化操作
│  │        >> 我们可以自己在此增加BeaDefition或者修改已经存在的BeanDefition对象
│  ├───beanfactorypostprocessor
│  │        >> 此对象是在BeanDefinitionRegistryPostProcessor对象之后执行，此处所有的BeanDefition也是还没有实例化的。
│  ├───beanpostprocessor
│  │        >> 对Bean调用初始化方法前后的处理。
│  │        >> 初始化方法（init-method、@PostConstruct、afterPropertiesSet）
│  ├─springboot-netty-websocket(SpringBoot中使用netty实现websocket)
│  │   >> 1、此处不使用SpringBoot官方提供的websocket实现，使用第三方实现 netty-websocket-spring-boot-starter
│  │   >> 2、此处的websocket是使用netty来实现的
│  │   >> 3、第三方jar包的地址为: https://gitee.com/Yeauty/netty-websocket-spring-boot-starter
│  ├─springboot-exception-handler [博客](https://blog.csdn.net/fu_huo_1993/article/details/124471001?spm=1001.2014.3001.5501)
│  │   >> 在SpringBoot中优雅的处理异常
│  ├─springboot-logger-split-file [博客](https://blog.csdn.net/fu_huo_1993/article/details/124606940?spm=1001.2014.3001.5501)
│  │   >> 实现系统中不同的模块日志，放置到不同到文件中。
│  │   >> 某些日志单独输出到一个文件中。
│  ├─springboot-logback-json-optimize-exception [博客](https://blog.csdn.net/fu_huo_1993/article/details/124658831?spm=1001.2014.3001.5501)
│  │   >> 优化异常堆栈信息
│  │   >> 以json的格式输出日志并优化异常堆栈信息
│  ├─springboot-record-bean-created-time-consuming
│  │   >> 计算每个Bean加载耗时多久 参考链接：https://juejin.cn/post/7117815437559070734#heading-9
│  ├─springboot-fastdfs [博客](https://blog.csdn.net/fu_huo_1993/article/details/127215157)
│  │   >> springboot整合fastdfs实现文件的上传和下载
│  │   >> 增加防盗链访问 [博客](https://blog.csdn.net/fu_huo_1993/article/details/127217790)
│  ├─sprngboot-xss
│  │   >> 在springboot中预防xss
│  ├─springboot-jackson
│  │   >> 使用 @JsonSerialize 注解实现自定义字段的序列化操作。
│  ├─springboot-ohc
│  │   >> 堆外内存的使用，借助 github 上的 ohc 来实现。
│  ├─springcloud-named-context-factory
│  │   >> 通过NamedContextFactory实现父子上下文
│  ├─springboot-dynamic-refresh-looger-level
│  │   >> 通过SpringBoot提供的LoggingSystem类来达到动态的刷新日志级别
├─es 
│  ├─es-api [api地址](https://www.elastic.co/guide/en/elasticsearch/client/java-rest/7.12/index.html)
│  │   >> es-api的简单使用。
│  │   >> 完成 geo_point api的简单使用。（com.huan.study.esapi.dslapi.geoapi.DistanceQueryApi）
│  │        >> 直接根据json字符串查询
│  │   >> 完成 简单的crud操作。（com.huan.study.esapi.documentapi.DocumentCrudApi）
│  │        >> 1、添加文档。
│  │        >> 2、替换文档（替换一整个文档，比如之前的文档10个字段，此处只给了2个字段，那么替换后的文档只有2个字段）。
│  │        >> 3、修改文档（修改部分字段）。
│  │        >> 4、删除文档。
│  │        >> 5、获取文档。
│  │        >> 6、只获取文档的_source部分的内容。
│  │        >> 7、判断文档是否存在。
│  │        >> 8、获取某个索引下所有的文档。
│  │   >> 使用function_score重新打分。（com.huan.study.esapi.dslapi.functionscore.FunctionScoreApi）
│  ├─es8-api 
│  │   >> 完成 批量插入操作。（com.huan.es8.documentapi.BulkApi）
│  │   >> 完成 高亮操作。（com.huan.es8.highlight.HighlightApiTest）
│  │   >> bucket 聚合
│  │        >> 完成 term聚合。（com.huan.es8.aggregations.bucket.TermsAggs）[博客](https://blog.csdn.net/fu_huo_1993/article/details/127822355)
│  │            >> 1、单字段聚合、聚合排序、多字段聚合、多个聚合
│  │            >> 2、脚本聚合
│  │            >> 3、子聚合、子聚合排序
│  │        >> 完成 multi terms聚合。（com.huan.es8.aggregations.bucket.MultiTermsAggs）[博客](https://blog.csdn.net/fu_huo_1993/article/details/127835403)
│  │            >> 1、多字段聚合实现 multi terms 、script、pipeline操作数据 聚合
│  │        >> 完成 rare terms聚合。（com.huan.es8.aggregations.bucket.RareTermsAggs） [博客](https://blog.csdn.net/fu_huo_1993/article/details/127840024)
│  │            >> 1、实现稀有term聚合，类似terms聚合中的根据_count asc排序，但是_count asc排序不准，需要使用rare terms聚合
│  │        >> 完成 histogram 聚合。（com.huan.es8.aggregations.bucket.HistogramAggs） [博客](https://blog.csdn.net/fu_huo_1993/article/details/128008665)
│  │            >> 1、实现直方图聚合
│  │        >> 完成 date histogram 聚合。（com.huan.es8.aggregations.bucket.DateHistogramAggs） [博客](https://blog.csdn.net/fu_huo_1993/article/details/128093563)
│  │            >> 1、实现日期直方图聚合
│  │        >> 完成 global、filters、cardinality 聚合。（com.huan.es8.aggregations.bucket.GlobalAndFiltersAggs） [博客](https://blog.csdn.net/fu_huo_1993/article/details/128262529)
│  │            >> 1. 全局聚合（不受查询条件的限制）
│  │            >> 2. 过滤聚合（对查询后的结果再次聚合）
│  │            >> 3. 基数聚合，类似SQL count(distinct(字段))，近似值
│  │        >> 完成 date range 聚合。（com.huan.es8.aggregations.bucket.DateRangeAggs）
│  │            >> 1. 日期范围聚合 [from,to)
│  │        >> 完成 bucket_sort 聚合。（com.huan.es8.aggregations.bucket.ScriptSortAggs）
│  │            >> 1. 聚合后分页、排序
│  │   >> metric 聚合
│  │        >> 完成 max,min,sum,avg,count(*),count(distinct),stat聚合
│  │        >> 完成 top hits 聚合 [博客](https://blog.csdn.net/fu_huo_1993/article/details/128452306)
│  │            >> 1. 大致实现如下功能，比如根据省聚合，可以得到每个省的统计，但是如何我想知道这个聚合下是那几个数据聚合来的，则可以使用 top_hits 聚合。
│  │            >> 2. 比如湖北省得到的聚合数字是5，此时我想知道湖北省下是那5个文档，那么就可以使用top hits。
│  │   >> pipeline 聚合
│  │        >> 完成 根据省分组获取省下最大的平均年龄 聚合
│  │        >> 完成 bucket_script 统计宝马车每个月销售率 聚合 [博客](https://blog.csdn.net/fu_huo_1993/article/details/128883683)
│  │   >> script 脚本
│  │        >> 完成脚本排序 （com.huan.es8.script.ScriptFieldSort） [博客](https://blog.csdn.net/fu_huo_1993/article/details/128606146)
│  │        >> script_fields 的使用 （com.huan.es8.script.ScriptFieldApi） [博客](https://blog.csdn.net/fu_huo_1993/article/details/128810117)
│  │        >> painless 脚本的一些应用场景 （com.huan.es8.script.painless.md） [博客](https://blog.csdn.net/fu_huo_1993/article/details/128857357)
│  │   >> suggesters 自动完成
│  │        >> 基于前缀的自动提示，使用 completion 来完成，这种类型的数据是保存在内存中的，速度快。 （com.huan.es8.suggesters.CompletionSuggesterApi） 
│  │        >> 基于上下文的前缀自动提示。 （com.huan.es8.suggesters.ContextCompletionSuggesterApi） 
│  │   >> nullvalue 处理字段为null或字段不存在的情况
│  │        >> 1. 使用null_value来处理
│  │            >> 1. null_value只可修饰 keyword,数字等类型，不可修饰 text类型。
│  │            >> 2. null_value 只影响数据的索引方式，它不会修改 _source 文档
│  │            >> 3. 一个空数组[]不包含明确的 null，因此不会被 null_value 替换。
│  │            >> 4. null_value 需要和 字段的 类型值 保持一致
│  │        >> 2. 使用bool->must_not->exists来处理
│  │            >> 1. 不依赖于字段的数据类型，无论字段的数据类型如何，它都会起作用
│  │            >> 2. []空数组类型的字段也会被搜索出来
│  │   >> runtimefield 运行时字段的使用 (runtime field可以替换script field)
│  │        >> 1. RuntimeFieldCorrectMappingError [博客](https://blog.csdn.net/fu_huo_1993/article/details/128840069)
│  │            >> 1. 使用 runtime field 来修正错误的mapping，比如一个字段映射的时候是text类型，此处通过runtime field来定一个keyword类型的字段
│  │        >> 2. MapRuntimeField 
│  │            >> 1. 使用 mapping 和 search request 的方式定义 runtime field
│  │            >> 2. 使用 mapping 删除 runtime field
│  │            >> 3. POST /index_script_fields/_mapping { "runtime": {"aggLineName": null}}
│  │        >> 3. RetrieveRuntimeFieldValue 
│  │            >> 1. 通过 fields api 获取 runtime field的值
│  │        >> 4. OverrideFieldByRuntimeField 
│  │            >> 1. 通过 runtime field 重写已有的字段， 即runtime field字段的名字和已有的字段名字一致
│  │        >> 5. IndexRuntimeField 
│  │            >> 1. 索引运行时字段，需要注意的一些事项
│  │   >> pipeline 管道的一些使用
│  │      >> 1. pipeline 的增删改查、测试和管道中应用管道
│  │   >> dslapi 
│  │      >> 1. PrefixQuery 前缀查询
├─mybatis 
│  ├─mybatis-typehandler-encrypt
│  │   >> 使用TypeHandler完成数据的加密和解密操作。
│  │   >> 完成 简单的crud操作。
├─sharding-jdbc
│  >> 实现对customer的分表操作。
│  >> 实现对customer_order使用复合分片算法，根据order_id和customer_id来实现。[博客](https://blog.csdn.net/fu_huo_1993/article/details/117258672?spm=1001.2014.3001.5501)
│  >> 实现 customer_order 和 customer_order_item 绑定表的查询，绑定表的分表规则需要一致。
│  >> 强制走主库(HitManager.getInstance().setMasterRouteOnly())。
├─canal 
│  ├─canal-api(canal client api的使用)
│  │   >> CanalClientApi（完成Canal Client Api的测试）。[博客](https://blog.csdn.net/fu_huo_1993/article/details/117469080?spm=1001.2014.3001.5501)
│  │   >> CanalClientApi（相同的代码启动多次，只有一个客户端可以获取到数据）。
│  ├─canal-kafka-consumer(kafka接收canal发送过来的消息)[博客](https://blog.csdn.net/fu_huo_1993/article/details/120075606)
├─security 
│  ├─authorization-server(Spring Authorization Server 的使用)[博客](https://blog.csdn.net/fu_huo_1993/article/details/118798160?spm=1001.2014.3001.5501)
│  ├   >> 1、完成授权码、客户端的授权
│  ├   >> 2、增加认证、授权异常处理
│  ├   >> 3、完成JWTtoken的个性化
│  ├   >> 4、为每个客户端都分配自己的公私钥，为每个客户端签名的JWT都使用自己的私钥
│  ├   >> 5、授权服务器使用固定的公私钥，参考文档 security/生成证书和导出公钥.md
│  ├─authorization-server-prod
│  ├   >> 1、完成授权码、客户端的授权
│  ├   >> 2、增加认证、授权异常处理
│  ├   >> 3、完成JWTtoken的个性化
│  ├   >> 4、为每个客户端都分配自己的公私钥，为每个客户端签名的JWT都使用自己的私钥
│  ├   >> 5、授权服务器使用固定的公私钥，参考文档 security/生成证书和导出公钥.md
│  ├─resource-server 资源服务器
│  ├   >> 1、完成资源服务器的搭建
│  ├   >> 2、完成异常的处理
│  ├   >> 3、完成解析JWT
│  ├        >> 1、从那个地方进行解析，请求头还是请求参数
│  ├        >> 2、对jwt进行校验
│  ├        >> 3、设置获取jwk的超时时间
│  ├        >> 4、给jwt的claim增加内容
│  ├        >> 5、设置从jwt的那个字段中获取权限
│  ├        >> 6、删除默认的SCOPE_权限前缀
│  ├        >> 7、资源服务器使用固定的公钥，参考文档 security/生成证书和导出公钥.md
├─seata(seata分布式事物的根项目) 
│  ├─seata-springboot-mybatis [博客](https://blog.csdn.net/fu_huo_1993/article/details/120454011?spm=1001.2014.3001.5501)
│  ├   >> 1、seata 整合 springboot 
│  ├   >> 2、使用seata-spring-boot-starter来整合
│  ├   >> 3、业务库需要有 undo_log 表，业务表需要有单一主键
│  ├   >> 4、AT模式下，数据源需要使用 DatasourceProxy 来代理
│  ├   >> 5、每个服务的事务分组可能不一样，但是需要和配置中心对应上。
│  ├        >> account-server 中的配置分组为：seata.tx-service-group=tx_account_service_group
│  ├        >> 配置中心必须存在 service.vgroupMapping.tx_account_service_group=default 配置项，default是集群，是服务端配置文件中指定的
│  ├──────account-server-50001
│  ├        >> 1、数据源使用 druid，使用自动数据源代理
│  ├──────order-service-50002
│  ├        >> 1、数据源使用 Hikari，不使用自定数据源的代理，自己配置配置数据源代理
│  ├        >> 2、使用 RestTemplate 来通讯，编写拦截器确保 xid 正常传递
│  ├        >> 3、在请求结束后，需要确保xid被清除了。
│  ├        >> 4、seata1.4中的bug,业务表中使用datetime类型的数据类型时报错，可以考虑使用timestamp类型
│  ├        >> 5、表中最好不要有自动更新时间戳的地方
│  ├─seata-springcloud-mybatis-plus [博客](https://blog.csdn.net/fu_huo_1993/article/details/120504020)
│  ├   >> 1、seata 整合 spring cloud 、mybatis-plus
│  ├   >> 2、微服务之间使用feign来调用，不用额外的配置feign传递xid，默认实现实现了
│  ├─seata-multiple-datasource [博客](https://blog.csdn.net/fu_huo_1993/article/details/120523329?spm=1001.2014.3001.5501)
│  ├   >> 1、seata 整合 多数据源。
│  ├   >> 2、需要注意一下 事物和数据源切换的 顺序问题。
│  ├─seata-springboot-rollback-tx [博客](https://blog.csdn.net/fu_huo_1993/article/details/120533658?spm=1001.2014.3001.5501)
│  ├   >> 1、实现分布式事物 在某些条件下手动回滚事物，比如: feign调用，出现了服务降级
│  ├   >> 2、实现分布式事物的挂起等操作等。比如：下单请求，在失败的时候，需要记录一个日志，此处记录日志的方法就不应该参与分布式事物。
│  ├─seata-springboot-failure-handler [博客](https://blog.csdn.net/fu_huo_1993/article/details/120655233?spm=1001.2014.3001.5501)
│  ├   >> 1、分布式事务失败时进行通知
│  ├─seata-springboot-kryo [博客](https://blog.csdn.net/fu_huo_1993/article/details/120706236)
│  ├   >> 1、undo_log的序列化方式修改成kryo，默认的jackson方式无法序列化datetime类型
│  ├─seata-srpingcloud-sharding-jdbc-druid-mybatis-plus
│  ├   >> ⚠️ 1、seata整合sharding-jdbc 整合失败，后期在完善。
├─redis(redis相关的操作) 
│  ├─redis-stream [博客](https://blog.csdn.net/fu_huo_1993/article/details/121276700)
│  ├   >> 1、使用 xread 读取流中的消息
│  ├   >> 2、消费组消费
│  ├   >> 3、对象类型传输
│  ├─redis-lua [博客](https://blog.csdn.net/fu_huo_1993/article/details/121001911)
│  ├   >> 1、在redis中使用lua脚本
│  ├─redis-pubsub
│  ├   >> 1、redis 发布订阅的一个小例子
│  ├─redis-bitmap-sign-in
│  ├   >> 1、基于bitmap实现的一个简单的签到功能
├─mini-project(一些小的测试项目) 
│  ├─juejin-auto-sign-in
│  ├   >> 1、掘金自动签到，抽奖，并发送邮件通知
│  ├─excel-api
│  ├   >> SameCellContentMerge 实现内容相同的单元格合并
├─drools(规则引擎的使用)
│  ├   >> 1、drools-metric 可以分析慢规则，但是不可在生产环境使用。
│  ├   >> 2、输出drools详细日志 <logger name="org.drools" level="debug"/>
│  ├   >> 3、不推荐使用System.out.println()来输出日志，推荐使用监听器或日志框架，参考链接 https://docs.drools.org/7.69.0.Final/drools-docs/html_single/index.html#engine-event-listeners-con_decision-engine
│  ├   >> 4、drools中是可以操作数据库，但是不推荐使用
│  ├─drools-quickstart [博客](https://blog.csdn.net/fu_huo_1993/article/details/124820766)
│  ├   >> 1、drools的入门案例,时间一个简单的打折例子
│  ├   >> 2、drools引擎的基本组件
│  ├   >> 3、drools debug日志的打印
│  ├─drools-session [博客](https://blog.csdn.net/fu_huo_1993/article/details/124751731)
│  ├   >> 1、drools 有状态和无状态session的理解
│  ├   >> 2、session级别的全局变量的使用
│  ├   >> 3、session 线程安全的控制，默认是线程安全，可以修改成非线程安全
│  ├─drools-session-pool [链接](https://docs.drools.org/7.69.0.Final/drools-docs/html_single/index.html#kie-sessions-pools-con_decision-engine)
│  ├   >> 1、使用session池化技术，提高性能
│  ├─drools-fact-equality-modes [博客](https://blog.csdn.net/fu_huo_1993/article/details/124766913)
│  ├   >> 1、多次向工作内存中插入 Fact 对象，看在不同的 equality modes 下，工作内存中会存在几个对象。
│  ├   >> 2、equalsBehavior在identity和equality下向工作内存中插入对象的不同结果
│  ├─drools-drl-function [博客](https://blog.csdn.net/fu_huo_1993/article/details/124773740)
│  ├   >> 1、实现在drools中自定义函数
│  ├   >> 2、then 阶段调用 drools 中自定义的函数
│  ├   >> 3、then 阶段也可以调用java中的静态方法，使用 import 类名，调用 类名.静态方法名()
│  ├   >> 4、when 中调用 drl function 或 java 静态方法，通过 eval来调用，不推荐使用，查看该项目下的README文件下方的链接
│  ├   >> 5、when中调用自定义function
│  ├─drools-drl-query [博客](https://blog.csdn.net/fu_huo_1993/article/details/124792736)
│  ├   >> 1、无参数query的使用 
│  ├   >> 2、有参数query的使用
│  ├   >> 3、java代码中`openLiveQuery`的使用（工作内存中的对象发生变化后，此查询可以立即知道）  
│  ├   >> 4、rule中使用query （需要注意查询时 加不加?的区别） rule xx when query01() ?query02() .. 是有区别的。
│  ├─drools-drl-type-declarations [博客](https://blog.csdn.net/fu_huo_1993/article/details/124726468)
│  ├   >> 1、在drl文件中声明一个枚举类型。    
│  ├   >> 2、在drl文件中声明一个类。  
│  ├   >> 3、在drl文件中声明一个类并完成继承操作。  
│  ├   >> 4、编写`rule`并使用我们自定义的type。  
│  ├   >> 5、java中给在drl文件中声明的type赋值，包括类和枚举类型
│  ├─drools-drl-global-variable [博客](https://blog.csdn.net/fu_huo_1993/article/details/124821295)
│  ├   >> 1、全局变量在规则文件中的使用。 
│  ├   >> 2、全局变量不会加入到工作内存，最好不要当作规则执行的条件，除非是常量。
│  ├   >> 3、全局变量可以用作规则执行完后，规则中返回结果的接收。
│  ├─drools-drl-rule-attributes [博客](https://blog.csdn.net/fu_huo_1993/article/details/124827412)
│  ├   >> 1、实现规则的优先级执行  salience.drl  
│  ├   >> 2、定义规则是否启用  enabled.drl  
│  ├   >> 3、定义规则在什么时间之后才可启用 date-effective.drl  
│  ├   >> 4、定义规则在什么时间之后被禁用 date-expires.drl
│  ├   >> 5、防止 `当前规则` fact修改后，不再次执行，别的规则修改fact,还会导致之前的规则执行。 no-loop.drl    
│  ├   >> 6、将激活的规则分组，只有获取焦点的组中的规则才会执行，但是`main`是默认组，也会执行。 agenda-group.drl  
│  ├   >> 7、activation-group 处于该分组中激活的规则，`同一个组下，只有一个规则可以执行`，其余的会被取消执行。但是别的组中激活的规则还是可以执行的。  activation-group.drl   
│  ├   >> 8、如果`在这个时间之后规则还成立`，那么执行该规则。duration.drl  
│  ├   >> 9、lock-on-active 同一个组中的多个规则，保证规则只会触发一次，即A规则导致B规则条件成立，如果B规则先执行过了，则B规则不会在执行。lock-on-active.drl
│  ├─drools-drl-no-loop-and-lock-on-active [博客](https://blog.csdn.net/fu_huo_1993/article/details/124843909)
│  ├   >> 1、死循环的解决  (no-loop true || lock-on-active true)  
│  ├   >> 2、同一个组中的规则绝对执行一次(lock-on-active true) 
│  ├─drools-drl-when [博客](https://blog.csdn.net/fu_huo_1993/article/details/124928724)
│  ├   >> 1、drools 中 when 的各种用法，包括操作符，一些方法等等
│  ├─drools-drl-then [博客](https://blog.csdn.net/fu_huo_1993/article/details/124944449)
│  ├   >> 1、更新工作内存中的fact对象 inset/insertLogical/modify/update/delete 等方法的使用
│  ├   >> 2、drools变量在drl文件中的使用。
│  ├   >> 3、规则的继承。
│  ├   >> 4、命名结果，可以实现规则的继承功能. do[..] then then[..]
│  ├   >> 5、then 中可以实现 if elseif else 的功能，需要注意 if 后面接 do 和 break的区别
│  ├─drools-invoked-specify-rule [博客](https://blog.csdn.net/fu_huo_1993/article/details/124949785)
│  ├   >> 1、执行指定的规则
│  ├─drools-termination-rule [博客](https://blog.csdn.net/fu_huo_1993/article/details/124961663)
│  ├   >> 1、假设存在多个规则，在某个规则执行完之后，终止之后的规则执行。drools.halt()
│  ├─drools-integrated-springboot [博客](https://blog.csdn.net/fu_huo_1993/article/details/124971190)
│  ├   >> 1、drools集成springboot
│  ├   >> 2、ksession中使用各种监听器
│  ├─drools-load-rule-from-string-or-file [博客](https://blog.csdn.net/fu_huo_1993/article/details/124983764)
│  ├   >> 1、从String字符串中加载Rule规则  
│  ├   >> 2、执行指定的规则文件
│  ├─drools-dynamic-crud-rule [博客](https://blog.csdn.net/fu_huo_1993/article/details/124998602)
│  ├   >> 1、规则的内容需要从数据库中动态加载出来，例子中是保存在内存中。  
│  ├   >> 2、需要创建多个`KieBase`，实现规则的隔离。  
│  ├   >> 3、可以动态的更新或添加规则。  
│  ├   >> 4、可以删除规则。 
│  ├   >> 5、默认情况下,KieBase是可以动态更新的KieSession的，这个是可以禁止的，通过mutability属性。
│  ├─drools-decision-table [博客](https://blog.csdn.net/fu_huo_1993/article/details/125035986)
│  ├   >> 1、决策表的简单使用。
│  ├   >> 2、决策表的语法，需要看上方的博客。
├─cola(COLA应用架构的简单使用)
│  ├─cola-customer
│  ├   >> 1、 
│  ├   >> 2、app层，完成服务的编排，不设置业务逻辑。
│  ├   >> 3、具体的逻辑逻辑在domain service中。
│  ├   >> 2、决策表的语法，需要看上方的博客。
├─spring-batch(Spring Batch框架的一些简单用法)
│  ├─spring-batch-read-csv [博客](https://blog.csdn.net/fu_huo_1993/article/details/126615140?spm=1001.2014.3001.5501)
│  ├   >> 1、 SpringBatch数据存数据库
│  ├   >> 2、 Job不自动运行 spring.batch.job.enabled=false
│  ├   >> 3、 ExecutionContext中如何传递参数 可以跨Step传参
│  ├   >> 4、 读取CSV文件内容并处理CSV文件
│  ├─spring-batch-parallel-steps
│  ├   >> 1、 多个Step之间并行执行，使用 Flow 和 TaskExecutor 来实现
├─hadoop(hadoop的一些简单使用)
│  ├─hdfs-api
│  ├   >> 1、 java hdfs api 的简单使用 [博客](https://blog.csdn.net/fu_huo_1993/article/details/129289894)
│  ├─mr-wordcount(word count案例)
│  ├   >> 基于MapReduce实现的word count案例
│  ├─mr-combiner(数据规约)(combiner不可乱用，乱用可能会得到错误的结果)
│  ├   >> combiner中文叫做数据规约，是MapReduce的一种优化手段。
│  ├   >> combiner的作用就是对Map端的输出先做一次局部合并，以减少在map和reduce节点之间的数据传输量。
│  ├   >> combiner是在每一个map task所在的节点本地运行，是局部聚合。
│  ├   >> reducer是对所有map task的输出结果计算，是全局聚合。
│  ├   >> 注意： combiner不可乱用，乱用可能会得到错误的结果。 （比如：如果算中位数或平均数则可能获取到的是错误的结果）
│  ├─mr-serializable(自定义序列化)
│  ├   >> 实现 key 和 value 的自定义序列化，以及 JavaBean 作为Key和Value
│  ├   >> JavaBean作为key时，必须要实现WritableComparable接口，否则报错。（如果同时实现Writeable和Comparable接口也不可以）
│  ├─mr-sort(自定义排序)
│  ├   >> 在mr中，涉及到排序的一般都是做为Key
│  ├─mr-partition(自定义分区)
│  ├   >> 自定义分区需要`Partitioner`类，<K,V>为map阶段输出的key和value
│  ├   >> 设置reduceTask的个数， reduceTask的个数需要和分区数一致
│  ├   >> 实现`Configurable`接口，获取`Configuration`对象
│  ├─mr-topn-group [博客](https://blog.csdn.net/fu_huo_1993/article/details/131701393)
│  ├   >> reduce阶段自定义分组
│  ├   >> 实现topN操作
│  ├   >> JavaBean作为Key进行排序
│  ├─mr-counter(自定义全局的计数器)
│  ├   >> 自定义全局的 Counter， 可用于MapReduce执行过程的一些信息监控
│  ├─mr-read-from-db(从数据库中读取数据)
│  ├   >> 从数据库中读取数据，并写入到文件中，不经过reduce阶段
│  ├─mr-write-to-db(写入数据到数据库中)
│  ├   >> 读取本地文件，并写入到数据库中
│  ├─mr-map-join(map端join操作)
│  ├   >> 基于Map端的Join操作， 实现将文件中的某些值，比如：（01，02）替换成另外一个文件中定义的中文值
│  ├─mr-compress(map reduce的压缩功能)
│  ├────mr-wordcount-map-bzip2-reduce-bzip2(bzip2压缩)
│  ├        >> 1、 基于 Map Reduce的 word count案例,并且 map 端的输出和reduce端的输出采用 bzip2压缩
│  ├─mr-read-multi-small-files(map端小文件处理)
│  ├   >> 基于CombineTextInputFormat优化map端小文件的处理
├─flink(flink的一些简单案例)
│  ├─flink-wordcount-dataset-api
│  ├   >> 1、基于DataSet Api实现一个简单的WordCount案例，DataSet Api已经不推荐使用，推荐使用DataStream Api
│  ├─flink-wordcount-datastream-api
│  ├   >> 1、基于DataStream Api实现一个简单的WordCount案例
│  ├   >> 2、默认是流的模式，可以通过Java代码中执行，也可以通过提交的时候使用 -Dexecution.runtime-mode=BATCH进行动态执行
│  ├   >> 3、environment.execute[Async]() 执行多次会产生多次Job
│  ├─flink-wordcount-lambda
│  ├   >> 1、在flink中使用Lambda
│  ├─flink-build-package
│  ├   >> 1、在flink程序中如何打包，修改作用域和使用shade插件。
│  ├   >> 2、flink依赖修改成provided之后，在程序中如何运行，请查看该项目下的readme.md文件。
│  ├─flink-operator-source
│  ├   >> 1、从集合中读取数据
│  ├   >> 2、从文件中读取数据
│  ├   >> 3、从socket中读取数据
│  ├   >> 4、自动生成数据
│  ├─flink-operator-transformation
│  ├   >> 1、map filter flatmap keyby reduce sum min max minBy maxBy RichXxxFunction 的使用
│  ├─flink-partition
│  ├   >> 1、flink 的各种自定的分区用法，比如：轮询、随机、广播等等
│  ├   >> 2、自定义分区，实现 Partitioner 接口
│  ├─flink-side-output
│  ├   >> 1、侧输出流的使用， 比如有一组数据，根据类型进行判断，需要分成多类，每种类型处理方式不一样，则可以使用side output
│  ├─flink-union-connect-stream
│  ├   >> 1、通过union将数据类型相同的各种流合并到一起
│  ├   >> 2、通过connect将数据类型不同的2个流合并到一起
│  ├─flink-sink(flink 的各种 sink 用法)
│  ├───flink-sink-mysql [参考连接](https://nightlies.apache.org/flink/flink-docs-release-1.17/docs/connectors/datastream/jdbc/)
│  ├      >> 1、flink将数据sink到mysql数据库中
│  ├───flink-sink-file [参考连接](https://nightlies.apache.org/flink/flink-docs-release-1.17/docs/connectors/datastream/filesystem/)
│  ├      >> 1、flink将数据sink到本地文件中
~~~
## 微信公众号

`更多内容请关注微信公众号`

![更多内容请关注微信公众号](./images/微信公众号.jpg)

##### 1、eureka的自我保护，生产环境中不建议关闭，测试环境中可以关闭。

##### 2、当使用了feign的 RequestInterceptor 向下游服务传递消息头，比如Cookie等时，如果启用了 Hystrix ,如果想在 RequestInterceptor 中使用到 ThreadLocal 中的值，必须要将隔离策略修改成信号量隔离。

##### 3、spring config client 的和注册中心的配置以及client config的配置必须要写在 bootstrap.yml 配置文件中
