# Spring 中异步的处理

可以使用这个实现 长轮训 操作。

## 1. 使用DeferredResult实现异步

```java

    /**
     * 查询订单支付结果
     *
     * @param orderId 订单编号
     * @return DeferredResult
     */
    @GetMapping("queryOrderPayResult")
    public DeferredResult<String> queryOrderPayResult(@RequestParam("orderId") String orderId) {
        log.info("订单orderId:[{}]发起了支付", orderId);
        ATOMIC_INTEGER.incrementAndGet();
        // // 3s 超时
        DeferredResult<String> result = new DeferredResult<>(3000L);
        // 超时操作
        result.onTimeout(() -> {
            DEFERRED_RESULT.get(orderId).setResult("超时了");
            log.info("订单orderId:[{}]发起支付,获取结果超时了.", orderId);
        });
        
        // 完成操作
        result.onCompletion(() -> {
            log.info("订单orderId:[{}]完成.", orderId);
            DEFERRED_RESULT.remove(orderId);
        });
        
        // 保存此 DeferredResult 的结果
        DEFERRED_RESULT.put(orderId, result);
        return result;
    }
    
    /**
     * 支付回调
     *
     * @param orderId 订单id
     * @return 支付回调结果
     */
    @GetMapping("payNotify")
    public String payNotify(@RequestParam("orderId") String orderId) {
        log.info("订单orderId:[{}]支付完成回调", orderId);
        
        // 默认结果发生了异常
        if ("123".equals(orderId)) {
            DEFERRED_RESULT.get(orderId).setErrorResult(new RuntimeException("订单发生了异常"));
            return "回调处理失败";
        }
        
        if (DEFERRED_RESULT.containsKey(orderId)) {
            DEFERRED_RESULT.get(orderId).setResult("完成支付");
            // 设置之前orderId toPay请求的结果
            return "回调处理成功";
        }
        return "回调处理失败";
    }
```

1. 方法的结果返回 DeferredResult  
2. 另外的线程通过设置 DeferredResult#setResult 来唤醒之前的线程  

## 2、注意事项

1. 异常处理可以使用 `@ExceptionHandler` 来处理。
2. 可以通过 `DeferredResultProcessingInterceptor` 和 `AsyncHandlerInterceptor` 来拦截异步方法执行步骤。
3. 使用 `DeferredResultProcessingInterceptor` 时，需要看方法上的注释，比如，超时方法，如果在调用了setResult方法则不会执行。

## 3、实现原理

1. Controller返回一个DeferredResult对象,并且把它保存在内在队列当中或者可以访问它的列表中。
2. Spring MVC开始异步处理.
3. DispatcherServlet与所有的Filter的Servlet容器线程退出,但Response仍然开放。
4. application通过多线程返回DeferredResult中sets值.并且Spring MVC分发request给Servlet容器.
5. DispatcherServlet再次被调用并且继续异步的处理产生的结果.

## 4、举例

### 1、需求

在系统的支付请求中一般分为2步，即先发送支付请求，然后查询交易结果(`queryOrderPayResult`)，然后支付回调(`payNotify`)告知成功还是失败。

### 2、实现步骤

1. 前端发送支付请求后，查询交易结果

```curl
curl -X GET http://localhost:8080/queryOrderPayResult\?orderId\=12345
```
此时前端的请求会挂起，只到3s后请求超时完成(如果支付回调没有完成)。

2. 模拟后端完成支付回调

```curl
curl -X GET http://localhost:8080/payNotify\?orderId\=12345 
```
调用完上方的请求后，可以看到上面的请求也完成了。

## 5、参考链接

1.[https://docs.spring.io/spring-framework/docs/current/reference/html/web.html#mvc-ann-async-deferredresult](https://docs.spring.io/spring-framework/docs/current/reference/html/web.html#mvc-ann-async-deferredresult)