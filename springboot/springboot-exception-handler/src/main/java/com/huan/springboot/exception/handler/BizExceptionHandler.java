package com.huan.springboot.exception.handler;

import com.huan.springboot.exception.BizException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

/**
 * BizException处理
 *
 * @author huan.fu
 * @date 2022/4/28 - 15:09
 */
@RestControllerAdvice
public class BizExceptionHandler {

    @ExceptionHandler(BizException.class)
    public ResponseEntity<Object> handleBizException(BizException exception) {
        // 自定义请求返回值
        Map<String, Object> body = new HashMap<>(4);
        body.put("code", "错误码");
        body.put("message", "异常信息为:" + exception.getMessage());
        return new ResponseEntity<>(body, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
