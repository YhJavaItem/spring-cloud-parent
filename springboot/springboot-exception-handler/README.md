# 介绍

此工程主要是为了处理 `SpringBoot` 程序中出现的问题。

# 知识点

1. 如果我们实现了`ResponseEntityExceptionHandler`类，那么Spring默认的异常处理将由`DefaultHandlerExceptionResolver`变成这个。
2. 如果我们返回的异常信息不需要自定义，则可以使用默认的`DefaultHandlerExceptionResolver`
3. 我们也可以使用 `@ExceptionHandler` 来进行特定的异常处理。