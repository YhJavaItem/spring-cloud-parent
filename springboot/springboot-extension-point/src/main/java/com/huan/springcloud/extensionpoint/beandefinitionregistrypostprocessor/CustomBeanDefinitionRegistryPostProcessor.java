package com.huan.springcloud.extensionpoint.beandefinitionregistrypostprocessor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * postProcessBeanDefinitionRegistry() 此方法所有的BeanDefinition都已经加载好了，但是没有实例化
 * 此处我们增加自己的BeanDefinition、或者修改已经存在的BeanDefinition
 *
 * @author huan.fu
 * @date 2022/5/30 - 16:22
 */
@Component
@Order(Ordered.LOWEST_PRECEDENCE)
public class CustomBeanDefinitionRegistryPostProcessor implements BeanDefinitionRegistryPostProcessor {

    private static final Logger log = LoggerFactory.getLogger(CustomBeanDefinitionRegistryPostProcessor.class);

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        BeanDefinition userServiceBeanDefinition = registry.getBeanDefinition("userService");
        // 动态给username字段赋值
        userServiceBeanDefinition.getPropertyValues().add("username", "张三");

        log.warn("postProcessBeanDefinitionRegistry");
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        log.warn("postProcessBeanFactory");
    }
}
