package com.huan.springcloud.autoconfiguration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;

/**
 * 此类模拟第三方提供的类，不在我们自己的SpringBoot扫描范围之内，通过第三方提供的自动装配来进行配置
 *
 * @author huan.fu
 * @date 2022/5/30 - 16:35
 */
public class AutoService {

    private static final Logger log = LoggerFactory.getLogger(AutoService.class);

    @PostConstruct
    public void init() {
        log.error("模拟的是第三方starter自动配置的类");
    }
}
