package com.huan.springcloud.extensionpoint.beanpostprocessor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

/**
 * 对Bean调用初始化方法前后的处理。
 * 初始化方法（init-method、@PostConstruct、afterPropertiesSet）
 *
 * @author huan.fu
 * @date 2022/5/30 - 16:06
 */
@Component
public class CustomBeanPostProcessor implements BeanPostProcessor {

    private static final Logger log = LoggerFactory.getLogger(CustomBeanPostProcessor.class);


    /**
     * 在调用Bean的初始化方法之前调用，初始化方法包括(init-method、@PostConstruct、afterPropertiesSet)等方法
     */
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof ServerService) {
            log.error("postProcessBeforeInitialization:[{}]", bean);
        }

        return BeanPostProcessor.super.postProcessBeforeInitialization(bean, beanName);
    }

    /**
     * 在调用Bean的初始化方法之后调用，初始化方法包括(init-method、@PostConstruct、afterPropertiesSet)等方法
     */
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof ServerService) {
            log.error("postProcessAfterInitialization:[{}]", bean);
        }
        return BeanPostProcessor.super.postProcessAfterInitialization(bean, beanName);
    }


}
