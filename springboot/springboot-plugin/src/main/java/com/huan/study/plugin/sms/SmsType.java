package com.huan.study.plugin.sms;

/**
 * @author huan.fu 2021/10/22 - 下午3:46
 */
public enum SmsType {
    /**
     * 阿里云渠道发送短信
     */
    ALIYUN("aliyun", "阿里云渠道发送短信"),
    /**
     * 腾讯云渠道发送短信
     */
    TENXUNYUN("tenxunyun", "腾讯云渠道发送短信");
    
    private final String smsChannel;
    private final String desc;
    
    SmsType(String smsChannel, String desc) {
        this.smsChannel = smsChannel;
        this.desc = desc;
    }
    
    public String getSmsChannel() {
        return smsChannel;
    }
    
    public String getDesc() {
        return desc;
    }
}
