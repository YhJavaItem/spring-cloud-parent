package com.huan.study.pointcut.controller;

import com.huan.study.pointcut.aop.Log;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 控制层
 *
 * @author huan.fu 2021/6/10 - 下午2:27
 */
@RestController
@Log
public class LogController {

    @GetMapping("log")
    @Log
    public String testLog(@RequestParam("param1") String param1, @RequestParam("param2") String param2) {
        return "return param1:" + param1 + " ,param2:" + param2;
    }
}
