package com.huan.springcloud.common;

/**
 * 默认的输出名字
 *
 * @author huan.fu
 * @date 2023/8/15 - 09:47
 */
public class DefaultCommonApi implements CommonApi {

    @Override
    public void showName() {
        System.out.println("default");
    }
}
