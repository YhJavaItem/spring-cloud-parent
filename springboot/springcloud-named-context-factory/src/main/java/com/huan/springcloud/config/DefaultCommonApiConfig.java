package com.huan.springcloud.config;

import com.huan.springcloud.common.CommonApi;
import com.huan.springcloud.common.DefaultCommonApi;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 默认的CommonApi实现
 *
 * @author huan.fu
 * @date 2023/8/15 - 09:56
 */
@Configuration
public class DefaultCommonApiConfig {

    @Bean
    @ConditionalOnMissingBean
    public CommonApi commonApi() {
        return new DefaultCommonApi();
    }
}
