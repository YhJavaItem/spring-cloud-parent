package com.huan.springcloud.common;

/**
 * 通用接口
 *
 * @author huan.fu
 * @date 2023/8/15 - 09:46
 */
public interface CommonApi {
    /**
     * 展示名字
     */
    void showName();

}
