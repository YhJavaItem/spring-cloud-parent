package com.huan.springcloud.config;

import org.springframework.cloud.context.named.NamedContextFactory;

import java.util.Arrays;
import java.util.Objects;

/**
 * 每个客户端一个配置
 *
 * @author huan.fu
 * @date 2023/8/15 - 09:36
 */
public class ClientSpecification implements NamedContextFactory.Specification {
    private final String name;
    private final Class<?>[] configuration;

    public ClientSpecification(String name, Class<?>[] configuration) {
        this.name = name;
        this.configuration = configuration;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Class<?>[] getConfiguration() {
        return configuration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ClientSpecification that = (ClientSpecification) o;
        return Objects.equals(name, that.name) && Arrays.equals(configuration, that.configuration);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name);
        result = 31 * result + Arrays.hashCode(configuration);
        return result;
    }

    @Override
    public String toString() {
        return "ClientSpecification{" +
                "name='" + name + '\'' +
                ", configuration=" + Arrays.toString(configuration) +
                '}';
    }
}
