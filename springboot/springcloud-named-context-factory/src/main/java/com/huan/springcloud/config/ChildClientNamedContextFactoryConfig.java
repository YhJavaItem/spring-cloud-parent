package com.huan.springcloud.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * 客户端自定义父子上下文配置
 *
 * @author huan.fu
 * @date 2023/8/15 - 09:53
 */
@Configuration
public class ChildClientNamedContextFactoryConfig {


    @Bean
    public ChildClientNamedContextFactory childClientNamedContextFactory(List<ClientSpecification> clientSpecificationList) {
        ChildClientNamedContextFactory factory = new ChildClientNamedContextFactory(DefaultCommonApiConfig.class);
        factory.setConfigurations(clientSpecificationList);
        return factory;
    }
}
