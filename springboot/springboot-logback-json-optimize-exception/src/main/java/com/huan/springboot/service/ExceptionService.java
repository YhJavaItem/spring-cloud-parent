package com.huan.springboot.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author huan.fu
 * @date 2022/5/6 - 15:56
 */
@Service
public class ExceptionService {

    private static final Logger log = LoggerFactory.getLogger(ExceptionService.class);

    public void exception(int num) {
        log.info("参数num:[{}]", num);

        if (num == 1) {
            try {
                int i = 1 / 0;
            } catch (Exception e) {
                log.error("发生了异常", e);
            }
        }

        if (num == 2) {
            try {
                int i = 1 / 0;
            } catch (Exception e) {
                log.error("发生了异常,当前num:[{}]", num, e);
            }
        }

        if (num == 3) {
            throw new IllegalArgumentException("当前传递的数字" + num + "不合法");
        }
    }

}
