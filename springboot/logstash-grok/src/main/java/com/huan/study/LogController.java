package com.huan.study;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Random;

/**
 * @author huan.fu 2021/5/13 - 上午8:56
 */
@RestController
public class LogController {
    public static Logger log = LoggerFactory.getLogger(LogController.class);

    @GetMapping("showLog")
    public void showLog(@RequestParam("type") int type, HttpServletRequest request) {
        switch (type) {
            case 1:
                log.info("显示日志,请求uri:[{}]", request.getRequestURI());
                break;
            case 3:
                try {
                    int j = 1 / 0;
                } catch (Exception e) {
                    log.error("请求:[{}]发生了异常", request.getRequestURI(), e);
                }
                break;
            case 4:
                add();
                break;
            default:
                log.info("默认的日志输出");
                break;
        }
    }

    private void add() {
        Random random = new Random();
        int first = random.nextInt(100);
        int second = random.nextInt(200);
        int result = first + second;
        log.info("加法计算 first:[{}] + second:[{}] = result:[{}]", first, second, result);
    }
}
