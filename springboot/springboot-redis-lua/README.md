# 实现功能

在 redis 中使用 lua 脚本

1. 单机分布式锁 com.huan.study.lua.LuaLock
2. 限流 com.huan.study.lua.Limit

## lua脚本的调试

> redis-cli --ldb --eval unlock.lua(脚本名称) key1(脚本中需要的key) , argv1 argv2(脚本中需要的argv)

- redis.breakpoint() 可以打一个断点
- 在lua debug 中输入h 可以获取到帮助信息
    * s 到当前行，但是还没有执行
    * c 到下个断点出
    * list 打印出lua代码
    * r 执行redis 命令 eg: r set key value

## 注意事项

1. lua脚本中,redis中需要用到的key都建议使用KEYS来获取，不然在集群模式下可能有问题.

```lua

-- 不好的方式,key此处是写死，不好，如果是通过hash方式执行，则修改一下脚本hash值也变化了,不好.
redis.call('set','key','value')

-- 推荐,通过 KEYS 来获取key
redis.call('set', KEYS[1], 'value') 

```

2. 需要注意,nil值的比较 需要使用 type(值) == 'nil' 来比较，即 nil需要加上引号。

3. 需要注意 lua 和 redis 的类型转换问题。


## 参考连接

1、[https://redis.io/commands/eval](https://redis.io/commands/eval)
2、[https://redis.io/topics/ldb](https://redis.io/topics/ldb)
3、[redis命令参考](http://doc.redisfans.com/index.html)