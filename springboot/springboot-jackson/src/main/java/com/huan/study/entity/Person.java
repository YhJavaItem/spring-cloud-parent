package com.huan.study.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.huan.study.jackson.*;

import java.math.BigDecimal;

/**
 * 用户类
 * 通过 @JsonSerialize 自定义序列化后的值
 *
 * @author huan.fu
 * @date 2023/1/14 - 23:45
 */
public class Person {

    private int id;

    @JsonSerialize(using = ReportDefaultJsonSerializer.class, nullsUsing = ReportDefaultJsonSerializer.class)
    private int oldId;

    private String name;
    @JsonSerialize(using = ReportDefaultJsonSerializer.class, nullsUsing = ReportDefaultJsonSerializer.class)
    private String address;

    private long age;
    @JsonSerialize(using = ReportDefaultJsonSerializer.class, nullsUsing = ReportDefaultJsonSerializer.class)
    private long oldAge;

    private BigDecimal money;
    @JsonSerialize(using = ReportDefaultJsonSerializer.class, nullsUsing = ReportDefaultJsonSerializer.class)
    private BigDecimal oldMoney;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOldId() {
        return oldId;
    }

    public void setOldId(int oldId) {
        this.oldId = oldId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }

    public long getOldAge() {
        return oldAge;
    }

    public void setOldAge(long oldAge) {
        this.oldAge = oldAge;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public BigDecimal getOldMoney() {
        return oldMoney;
    }

    public void setOldMoney(BigDecimal oldMoney) {
        this.oldMoney = oldMoney;
    }
}
