package com.huan.springboot.qq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

/**
 * 模拟QQ服务
 *
 * @author huan.fu
 * @date 2022/5/6 - 11:24
 */
@Component
public class QQService {

    private static final Logger log = LoggerFactory.getLogger(QQService.class);

    // getLogger("qqLoginName") 里的 qqLoginName 需要和 logback-spring.xml 中 logger的name一致，才会应用
    private static final Logger qqLoginName = LoggerFactory.getLogger("qqLoginName");

    public void login(String loginName) {
        log.info("QQ业务: 用户:[{}]开始使用QQ来登录系统", loginName);

        MDC.put("CLASSNAME", QQService.class.getName());
        qqLoginName.info("登录用户:[{}]", loginName);
    }
}
