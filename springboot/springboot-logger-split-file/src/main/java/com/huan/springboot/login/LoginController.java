package com.huan.springboot.login;

import com.huan.springboot.qq.QQService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 模拟登录控制器
 *
 * @author huan.fu
 * @date 2022/5/6 - 11:23
 */
@RestController
public class LoginController {

    private static final Logger log = LoggerFactory.getLogger(LoginController.class);

    @Resource
    private QQService qqService;

    @GetMapping("login/{loginName}")
    public String login(@PathVariable("loginName") String loginName) {
        log.info("自己业务：用户:[{}]进行登录", loginName);
        qqService.login(loginName);
        return "ok";
    }
}
