package com.huan.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoadBeanCostApplication {

    public static void main(String[] args) {
        SpringApplication.run(LoadBeanCostApplication.class, args);
    }
}
