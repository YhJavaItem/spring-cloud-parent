package com.huan.study.context.product;

import com.huan.study.context.CommonApi;

/**
 * @author huan.fu 2021/6/8 - 下午2:25
 */
public class ProductApi implements CommonApi {
    @Override
    public String apiName() {
        return "product - api";
    }
}
