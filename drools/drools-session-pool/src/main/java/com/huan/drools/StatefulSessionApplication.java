package com.huan.drools;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieContainerSessionsPool;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;

/**
 * 有状态session测试
 *
 * @author huan.fu
 * @date 2022/5/13 - 10:27
 */
public class StatefulSessionApplication {

    public static void main(String[] args) {
        KieServices kieServices = KieServices.get();
        KieContainer kieClasspathContainer = kieServices.getKieClasspathContainer();
        // 初始化 10 个session，如果在运行的过程中，需要超过10个，则会自己动态的扩展
        KieContainerSessionsPool sessionsPool = kieClasspathContainer.newKieSessionsPool(10);

        KieSession kieSession = sessionsPool.newKieSession("stateful-session");

        Counter counter1 = new Counter("count-01", 0);
        kieSession.insert(counter1);
        // 触发规则
        kieSession.fireAllRules();

        // 有状态的Session最后一定要调用此方法，防止内存泄漏，此时session不会销毁，会再次加入到池中
        kieSession.dispose();

        // 关闭session pool 防止内存泄漏 sessionsPool.shutdown(); 或者  kieClasspathContainer.dispose(); 都可以
        sessionsPool.shutdown();
    }
}
