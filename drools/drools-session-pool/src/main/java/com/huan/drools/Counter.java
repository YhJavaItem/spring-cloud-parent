package com.huan.drools;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 计数器
 *
 * @author huan.fu
 * @date 2022/5/13 - 13:02
 */
@Data
@AllArgsConstructor
public class Counter {
    /**
     * 名称
     */
    private String name;
    /**
     * 计数
     */
    private Integer cnt;
}
