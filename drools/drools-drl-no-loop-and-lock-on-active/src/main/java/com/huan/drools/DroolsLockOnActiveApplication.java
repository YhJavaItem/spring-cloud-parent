package com.huan.drools;

import org.drools.core.base.RuleNameStartsWithAgendaFilter;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

/**
 * drools 测试类
 */
public class DroolsLockOnActiveApplication {
    public static void main(String[] args) {
        KieServices kieServices = KieServices.get();
        KieContainer kieContainer = kieServices.getKieClasspathContainer();
        KieSession kieSession = kieContainer.newKieSession("ksession");

        kieSession.getAgenda().getAgendaGroup("group-002").setFocus();

        Customer customer = new Customer();
        customer.setScore(70);

        kieSession.insert(customer);
        kieSession.fireAllRules(new RuleNameStartsWithAgendaFilter("lock_on_active_"));

        kieSession.dispose();
    }
}
