package com.huan.drools.fact;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author huan.fu
 * @date 2022/5/14 - 19:25
 */
@Data
@AllArgsConstructor
public class Person {
    private String name;
    private Integer age;
    // 是否可以玩游戏，此字段的值，由 drools 引擎计算得出
    private Boolean canPlayGame;
}
