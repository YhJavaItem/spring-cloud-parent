package com.huan.drools;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author huan.fu
 * @date 2022/5/14 - 19:25
 */
@Data
@AllArgsConstructor
public class Person {
    private String name;
    private Integer age;
    private Date registerDate;
    private List<String> hobbyList;
    private Map<String, String> map;
    private Car car;

    public boolean isChild() {
        // 是否是成年人，注意这个方法需要是无状态的，因为drools中会用到。drools中为了提高效率，会进行缓存，如果方法中出现 int i = j++;
        return this.age < 18;
    }
}
