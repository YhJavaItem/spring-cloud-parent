package com.huan.drools;

import lombok.*;

/**
 * 车
 *
 * @author huan.fu
 * @date 2022/5/23 - 15:40
 */
@Getter
@Setter
public class Car {
    private String name;
    private String color;
}

