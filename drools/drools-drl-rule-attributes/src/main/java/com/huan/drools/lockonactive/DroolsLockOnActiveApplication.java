package com.huan.drools.lockonactive;

import org.drools.core.base.RuleNameStartsWithAgendaFilter;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

/**
 * lock-on-active 限制组中的规则只执行一次
 */
public class DroolsLockOnActiveApplication {
    public static void main(String[] args) {
        System.setProperty("drools.dateformat", "yyyy-MM-dd HH:mm:ss");
        KieServices kieServices = KieServices.get();
        KieContainer kieContainer = kieServices.getKieClasspathContainer();
        KieSession kieSession = kieContainer.newKieSession("rule-attributes-ksession");
        // 激活组
        kieSession.getAgenda().getAgendaGroup("group-001").setFocus();

        Person person = new Person("张三", 20);
        kieSession.insert(person);

        // 只匹配规则名称是已 lock_on_active_ 开头的规则，忽略其余的规则
        kieSession.fireAllRules(new RuleNameStartsWithAgendaFilter("lock_on_active_"));
        kieSession.dispose();
    }
}
