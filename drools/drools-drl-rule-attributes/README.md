# 实现功能

1、实现规则的优先级执行  salience.drl  
2、定义规则是否启用  enabled.drl  
3、定义规则在什么时间之后才可启用 date-effective.drl  
4、定义规则在什么时间之后被禁用 date-expires.drl
5、防止 `当前规则` fact修改后，不再次执行，别的规则修改fact,还会导致之前的规则执行。 no-loop.drl    
6、将激活的规则分组，只有获取焦点的组中的规则才会执行，但是`main`是默认组，也会执行。 agenda-group.drl  
7、activation-group 处于该分组中激活的规则，`同一个组下，只有一个规则可以执行`，其余的会被取消执行。但是别的组中激活的规则还是可以执行的。  activation-group.drl   
8、如果`在这个时间之后规则还成立`，那么执行该规则。duration.drl  
9、lock-on-active 同一个组中的多个规则，保证规则只会触发一次，即A规则导致B规则条件成立，如果B规则先执行过了，则B规则不会在执行。lock-on-active.drl

执行指定的规则`kieSession.fireAllRules(new RuleNameStartsWithAgendaFilter("salience_"))`

# 博客地址
[https://blog.csdn.net/fu_huo_1993/article/details/124827412](https://blog.csdn.net/fu_huo_1993/article/details/124827412)

# 参考链接
1、[https://stackoverflow.com/questions/6870192/understanding-agenda-group-in-drools](https://stackoverflow.com/questions/6870192/understanding-agenda-group-in-drools)
2、[https://github.com/kiegroup/drools/blob/main/drools-compiler/src/main/java/org/drools/compiler/rule/builder/RuleBuilder.java#L176](https://github.com/kiegroup/drools/blob/main/drools-compiler/src/main/java/org/drools/compiler/rule/builder/RuleBuilder.java#L176)

# 注意事项
1、no-loop 和 lock-on-active 有什么不同呢？  
我的理解：  
`no-loop：`表达的是当前规则的`RHS`部分对Fact对象的修改，不会导致当前规则重新执行，但是如果是别的规则修改了
           `Fact`对象，即使当前规则的`no-loop true`还是会导致该规则的触发。  
`lock-on-active` 保证当前规则只会执行一次，即使后期别的规则修改了`Fact`对象，该规则是不会再次被触发的。
2、agenda-group  
agenda-group 的数据结构类似与栈，激活的组会被放置在栈顶，
`main`是默认组，总是存在的，即没有配置agenda-group的就是`main`，
`main`总是会执行的。
[https://stackoverflow.com/questions/6870192/understanding-agenda-group-in-drools](https://stackoverflow.com/questions/6870192/understanding-agenda-group-in-drools)

