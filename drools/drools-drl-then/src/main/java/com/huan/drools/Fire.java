package com.huan.drools;

import lombok.Getter;
import lombok.Setter;

/**
 * 火
 *
 * @author huan.fu
 * @date 2022/5/24 - 12:17
 */
@Getter
@Setter
public class Fire {
    // 火灾的名字
    private String name;
}
