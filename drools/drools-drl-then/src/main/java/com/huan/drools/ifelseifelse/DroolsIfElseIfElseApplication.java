package com.huan.drools.ifelseifelse;

import com.huan.drools.Car;
import com.huan.drools.Customer;
import org.drools.core.base.RuleNameEqualsAgendaFilter;
import org.drools.core.event.DebugProcessEventListener;
import org.kie.api.KieServices;
import org.kie.api.event.rule.DebugAgendaEventListener;
import org.kie.api.event.rule.DebugRuleRuntimeEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

/**
 * when if else if
 */
public class DroolsIfElseIfElseApplication {
    public static void main(String[] args) {
        KieServices kieServices = KieServices.get();
        KieContainer kieContainer = kieServices.getKieClasspathContainer();
        KieSession kieSession = kieContainer.newKieSession("then-ksession");
        kieSession.addEventListener(new DebugRuleRuntimeEventListener());
        kieSession.addEventListener(new DebugProcessEventListener());
        kieSession.addEventListener(new DebugAgendaEventListener());

        // 客户年龄小于60岁(kieSession); // 规则不符合-没有内容输出

        // 客户年龄大于60岁并且level是1无车(kieSession); // 输出 level1

        // 客户年龄大于60岁并且level是1有车(kieSession); // 输出 level1 我执行了

        // 客户年龄大于60岁并且level是2无车(kieSession); // 输出 level2
        // 客户年龄大于60岁并且level是2有车(kieSession); // 输出 level2

        // 客户年龄大于60岁并且level是3无车(kieSession); // 输出 levelOther
        客户年龄大于60岁并且level是3有车(kieSession); // 输出 levelOther 我执行了

        kieSession.fireAllRules(new RuleNameEqualsAgendaFilter("if else-if else"));


        kieSession.dispose();
    }

    public static void 客户年龄大于60岁并且level是3有车(KieSession session) {
        Customer customer = new Customer();
        customer.setAge(100);
        customer.setLevel(3);
        session.insert(customer);
        session.insert(new Car());
    }

    public static void 客户年龄大于60岁并且level是3无车(KieSession session) {
        Customer customer = new Customer();
        customer.setAge(100);
        customer.setLevel(3);
        session.insert(customer);
    }

    public static void 客户年龄大于60岁并且level是2有车(KieSession session) {
        Customer customer = new Customer();
        customer.setAge(100);
        customer.setLevel(2);
        session.insert(customer);
        session.insert(new Car());
    }

    public static void 客户年龄大于60岁并且level是2无车(KieSession session) {
        Customer customer = new Customer();
        customer.setAge(100);
        customer.setLevel(2);
        session.insert(customer);
    }

    public static void 客户年龄大于60岁并且level是1有车(KieSession session) {
        Customer customer = new Customer();
        customer.setAge(100);
        customer.setLevel(1);
        session.insert(customer);
        session.insert(new Car());
    }

    public static void 客户年龄大于60岁并且level是1无车(KieSession session) {
        Customer customer = new Customer();
        customer.setAge(100);
        customer.setLevel(1);
        session.insert(customer);
    }

    public static void 客户年龄小于60岁(KieSession session) {
        Customer customer = new Customer();
        customer.setAge(50);
        session.insert(customer);
    }

}
