package com.huan.drools.insertmethod;

import com.huan.drools.Fire;
import org.drools.core.base.RuleNameStartsWithAgendaFilter;
import org.kie.api.KieServices;
import org.kie.api.event.rule.DebugRuleRuntimeEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

/**
 * drools insert方法
 * 1. `insert`可以向工作内存中插入`Fact`对象。
 * 2. `insert`方法调用后，会导致模式的重新匹配，导致之前不会执行的规则，重新执行。
 * 3. `insert`方法插入到工作内存的对象，在规则不成立时，不会自动删除，需要手动删除，注意和`insertLogical`的区别
 */
public class DroolsInsertApplication {
    public static void main(String[] args) {
        KieServices kieServices = KieServices.get();
        KieContainer kieContainer = kieServices.getKieClasspathContainer();
        KieSession kieSession = kieContainer.newKieSession("then-ksession");
        kieSession.addEventListener(new DebugRuleRuntimeEventListener());

        kieSession.insert(new Fire());
        kieSession.fireAllRules(new RuleNameStartsWithAgendaFilter("insert_"));

        kieSession.dispose();
    }
}
