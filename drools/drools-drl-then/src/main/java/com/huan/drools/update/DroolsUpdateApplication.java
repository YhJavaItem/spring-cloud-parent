package com.huan.drools.update;

import com.huan.drools.Fire;
import org.drools.core.base.RuleNameStartsWithAgendaFilter;
import org.kie.api.KieServices;
import org.kie.api.event.rule.DebugRuleRuntimeEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

/**
 * drools update方法（更新工作内存中对象的值-推荐使用modify方法）
 */
public class DroolsUpdateApplication {
    public static void main(String[] args) {
        KieServices kieServices = KieServices.get();
        KieContainer kieContainer = kieServices.getKieClasspathContainer();
        KieSession kieSession = kieContainer.newKieSession("then-ksession");
        kieSession.addEventListener(new DebugRuleRuntimeEventListener());

        kieSession.insert(new Fire());
        kieSession.fireAllRules(new RuleNameStartsWithAgendaFilter("update_"));

        kieSession.dispose();
    }
}
