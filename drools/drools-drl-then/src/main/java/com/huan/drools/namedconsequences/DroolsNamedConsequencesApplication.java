package com.huan.drools.namedconsequences;

import com.huan.drools.Car;
import com.huan.drools.Customer;
import org.drools.core.base.RuleNameStartsWithAgendaFilter;
import org.kie.api.KieServices;
import org.kie.api.event.rule.DebugRuleRuntimeEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

/**
 * 命令结果测试，do[...]
 */
public class DroolsNamedConsequencesApplication {
    public static void main(String[] args) {
        KieServices kieServices = KieServices.get();
        KieContainer kieContainer = kieServices.getKieClasspathContainer();
        KieSession kieSession = kieContainer.newKieSession("then-ksession");
        kieSession.addEventListener(new DebugRuleRuntimeEventListener());

        Car car = new Car();
        Customer customer = new Customer();
        customer.setAge(65);
        kieSession.insert(customer);
        kieSession.insert(car);
        kieSession.fireAllRules(new RuleNameStartsWithAgendaFilter("命名结果_"));

        kieSession.dispose();
    }
}
