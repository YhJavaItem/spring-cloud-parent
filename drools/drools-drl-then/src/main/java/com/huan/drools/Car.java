package com.huan.drools;

import lombok.Getter;
import lombok.Setter;

/**
 * 车
 * @author huan.fu
 * @date 2022/5/24 - 14:07
 */
@Getter
@Setter
public class Car {
    private Boolean freeParking;
}
