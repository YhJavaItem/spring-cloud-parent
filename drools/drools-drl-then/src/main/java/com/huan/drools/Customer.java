package com.huan.drools;

import lombok.Getter;
import lombok.Setter;

/**
 * 客户类
 *
 * @author huan.fu
 * @date 2022/5/24 - 14:06
 */

@Getter
@Setter
public class Customer {
    private Integer age;
    private Double discount;
    private Integer level;
}
