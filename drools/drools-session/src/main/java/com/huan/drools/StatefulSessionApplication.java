package com.huan.drools;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;

/**
 * 有状态session测试
 *
 * @author huan.fu
 * @date 2022/5/13 - 10:27
 */
public class StatefulSessionApplication {

    public static void main(String[] args) {
        KieServices kieServices = KieServices.get();
        KieContainer kieClasspathContainer = kieServices.getKieClasspathContainer();
        KieSession kieSession = kieClasspathContainer.newKieSession("stateful-session");

        Counter counter1 = new Counter("count-01", 0);
        FactHandle factHandle = kieSession.insert(counter1);
        // 触发规则
        kieSession.fireAllRules();

        Counter counter2 = new Counter("count-02", 0);
        kieSession.insert(counter2);

        // 这个操作，将会导致规则的重新匹配
        kieSession.update(factHandle, counter1);

        // 再次触发规则
        kieSession.fireAllRules();

        // 有状态的Session最后一定要调用此方法，防止内存泄漏
        kieSession.dispose();
    }
}
