# 实现功能
在规则中实现全局变量的使用。

# 注意事项
1. 使用`global`来定义全局变量，它可以为规则`提供数据和服务`。
2. 全局变量并`不会被写入到工作内存`中，因此我们`不可放到规则的约束条件`中，即`when`的后面，`除非这个全局变量是常量`。
3. `如果不同的包中存在相同标识符的常量`，那么我们的常量必须是相同的类型，以便可以引用到相同的值。`官网原文：` <sub>If you declare global variables with the same identifier in multiple packages, then you must set all the packages with the same type so that they all reference the same global value.</sub>
4. 尽量不要使用全局变量在规则之间传递参数，如果需要在规则之间传递参数，我们应该向工作内存中插入`Fact`对象。
5. 全局变量可以用来接收规则执行后的结果

# 博客地址
[https://blog.csdn.net/fu_huo_1993/article/details/124821295](https://blog.csdn.net/fu_huo_1993/article/details/124821295)

# 参考链接
[https://docs.drools.org/7.69.0.Final/drools-docs/html_single/index.html#drl-globals-con_drl-rules](https://docs.drools.org/7.69.0.Final/drools-docs/html_single/index.html#drl-globals-con_drl-rules)
