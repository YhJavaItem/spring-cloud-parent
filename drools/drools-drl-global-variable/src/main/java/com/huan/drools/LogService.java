package com.huan.drools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 定义一个日志服务，将做为全局变量在drl中使用
 *
 * @author huan.fu
 * @date 2022/5/17 - 15:17
 */
public class LogService {

    private static final Logger log = LoggerFactory.getLogger(LogService.class);
    public void insertLog(String username) {
        log.info("insertLog日志方法调用了,username:[{}]", username);
    }
}
