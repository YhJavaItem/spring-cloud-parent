```
├─drools(规则引擎的使用)
│  ├   >> 1、drools-metric 可以分析慢规则，但是不可在生产环境使用。
│  ├   >> 2、输出drools详细日志 <logger name="org.drools" level="debug"/>
│  ├   >> 3、不推荐使用System.out.println()来输出日志，推荐使用监听器或日志框架，参考链接 https://docs.drools.org/7.69.0.Final/drools-docs/html_single/index.html#engine-event-listeners-con_decision-engine
│  ├   >> 4、drools中是可以操作数据库，但是不推荐使用
│  ├─drools-quickstart [博客](https://blog.csdn.net/fu_huo_1993/article/details/124820766)
│  ├   >> 1、drools的入门案例,时间一个简单的打折例子
│  ├   >> 2、drools引擎的基本组件
│  ├   >> 3、drools debug日志的打印
│  ├─drools-session [博客](https://blog.csdn.net/fu_huo_1993/article/details/124751731)
│  ├   >> 1、drools 有状态和无状态session的理解
│  ├   >> 2、session级别的全局变量的使用
│  ├   >> 3、session 线程安全的控制，默认是线程安全，可以修改成非线程安全
│  ├─drools-session-pool [链接](https://docs.drools.org/7.69.0.Final/drools-docs/html_single/index.html#kie-sessions-pools-con_decision-engine)
│  ├   >> 1、使用session池化技术，提高性能
│  ├─drools-fact-equality-modes [博客](https://blog.csdn.net/fu_huo_1993/article/details/124766913)
│  ├   >> 1、多次向工作内存中插入 Fact 对象，看在不同的 equality modes 下，工作内存中会存在几个对象。
│  ├   >> 2、equalsBehavior在identity和equality下向工作内存中插入对象的不同结果
│  ├─drools-drl-function [博客](https://blog.csdn.net/fu_huo_1993/article/details/124773740)
│  ├   >> 1、实现在drools中自定义函数
│  ├   >> 2、then 阶段调用 drools 中自定义的函数
│  ├   >> 3、then 阶段也可以调用java中的静态方法，使用 import 类名，调用 类名.静态方法名()
│  ├   >> 4、when 中调用 drl function 或 java 静态方法，通过 eval来调用，不推荐使用，查看该项目下的README文件下方的链接
│  ├   >> 5、when中调用自定义function
│  ├─drools-drl-query [博客](https://blog.csdn.net/fu_huo_1993/article/details/124792736)
│  ├   >> 1、无参数query的使用
│  ├   >> 2、有参数query的使用
│  ├   >> 3、java代码中`openLiveQuery`的使用（工作内存中的对象发生变化后，此查询可以立即知道）  
│  ├   >> 4、rule中使用query （需要注意查询时 加不加?的区别） rule xx when query01() ?query02() .. 是有区别的。
│  ├─drools-drl-type-declarations [博客](https://blog.csdn.net/fu_huo_1993/article/details/124726468)
│  ├   >> 1、在drl文件中声明一个枚举类型。    
│  ├   >> 2、在drl文件中声明一个类。  
│  ├   >> 3、在drl文件中声明一个类并完成继承操作。  
│  ├   >> 4、编写`rule`并使用我们自定义的type。  
│  ├   >> 5、java中给在drl文件中声明的type赋值，包括类和枚举类型
│  ├─drools-drl-global-variable [博客](https://blog.csdn.net/fu_huo_1993/article/details/124821295)
│  ├   >> 1、全局变量在规则文件中的使用。
│  ├   >> 2、全局变量不会加入到工作内存，最好不要当作规则执行的条件，除非是常量。
│  ├   >> 3、全局变量可以用作规则执行完后，规则中返回结果的接收。
│  ├─drools-drl-rule-attributes [博客](https://blog.csdn.net/fu_huo_1993/article/details/124827412)
│  ├   >> 1、实现规则的优先级执行  salience.drl  
│  ├   >> 2、定义规则是否启用  enabled.drl  
│  ├   >> 3、定义规则在什么时间之后才可启用 date-effective.drl  
│  ├   >> 4、定义规则在什么时间之后被禁用 date-expires.drl
│  ├   >> 5、防止 `当前规则` fact修改后，不再次执行，别的规则修改fact,还会导致之前的规则执行。 no-loop.drl    
│  ├   >> 6、将激活的规则分组，只有获取焦点的组中的规则才会执行，但是`main`是默认组，也会执行。 agenda-group.drl  
│  ├   >> 7、activation-group 处于该分组中激活的规则，`同一个组下，只有一个规则可以执行`，其余的会被取消执行。但是别的组中激活的规则还是可以执行的。  activation-group.drl   
│  ├   >> 8、如果`在这个时间之后规则还成立`，那么执行该规则。duration.drl  
│  ├   >> 9、lock-on-active 同一个组中的多个规则，保证规则只会触发一次，即A规则导致B规则条件成立，如果B规则先执行过了，则B规则不会在执行。lock-on-active.drl
│  ├─drools-drl-no-loop-and-lock-on-active [博客](https://blog.csdn.net/fu_huo_1993/article/details/124843909)
│  ├   >> 1、死循环的解决  (no-loop true || lock-on-active true)  
│  ├   >> 2、同一个组中的规则绝对执行一次(lock-on-active true)
│  ├─drools-drl-when [博客](https://blog.csdn.net/fu_huo_1993/article/details/124928724)
│  ├   >> 1、drools 中 when 的各种用法，包括操作符，一些方法等等
│  ├─drools-drl-then [博客](https://blog.csdn.net/fu_huo_1993/article/details/124944449)
│  ├   >> 1、更新工作内存中的fact对象 inset/insertLogical/modify/update/delete 等方法的使用
│  ├   >> 2、drools变量在drl文件中的使用。
│  ├   >> 3、规则的继承。
│  ├   >> 4、命名结果，可以实现规则的继承功能. do[..] then then[..]
│  ├   >> 5、then 中可以实现 if elseif else 的功能，需要注意 if 后面接 do 和 break的区别
│  ├─drools-invoked-specify-rule [博客](https://blog.csdn.net/fu_huo_1993/article/details/124949785)
│  ├   >> 1、执行指定的规则
│  ├─drools-termination-rule [博客](https://blog.csdn.net/fu_huo_1993/article/details/124961663)
│  ├   >> 1、假设存在多个规则，在某个规则执行完之后，终止之后的规则执行。drools.halt()
│  ├─drools-integrated-springboot [博客](https://blog.csdn.net/fu_huo_1993/article/details/124971190)
│  ├   >> 1、drools集成springboot
│  ├   >> 2、ksession中使用各种监听器
│  ├─drools-load-rule-from-string-or-file [博客](https://blog.csdn.net/fu_huo_1993/article/details/124983764)
│  ├   >> 1、从String字符串中加载Rule规则  
│  ├   >> 2、执行指定的规则文件
│  ├─drools-dynamic-crud-rule [博客](https://blog.csdn.net/fu_huo_1993/article/details/124998602)
│  ├   >> 1、规则的内容需要从数据库中动态加载出来，例子中是保存在内存中。  
│  ├   >> 2、需要创建多个`KieBase`，实现规则的隔离。  
│  ├   >> 3、可以动态的更新或添加规则。  
│  ├   >> 4、可以删除规则。
│  ├   >> 5、默认情况下,KieBase是可以动态更新的KieSession的，这个是可以禁止的，通过mutability属性。
│  ├─drools-decision-table [博客](https://blog.csdn.net/fu_huo_1993/article/details/125035986)
│  ├   >> 1、决策表的简单使用。
│  ├   >> 2、决策表的语法，需要看上方的博客。
``` 