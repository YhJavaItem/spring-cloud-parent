package com.huan.seata;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author huan.fu 2021/9/26 - 下午4:22
 */
@SpringBootApplication
@MapperScan(basePackages = "com.huan.seata.mapper")
public class AccountService50012Application {
    
    public static void main(String[] args) {
        SpringApplication.run(AccountService50012Application.class);
    }
}
