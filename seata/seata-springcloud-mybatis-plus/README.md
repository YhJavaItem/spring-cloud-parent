# 集成步骤

1、引入jar包
```xml
    <dependency>
        <groupId>com.alibaba.cloud</groupId>
        <artifactId>spring-cloud-starter-alibaba-seata</artifactId>
        <version>2021.1</version>
        <exclusions>
            <exclusion>
                <artifactId>seata-spring-boot-starter</artifactId>
                <groupId>io.seata</groupId>
            </exclusion>
        </exclusions>
    </dependency>
    <dependency>
        <groupId>io.seata</groupId>
        <artifactId>seata-spring-boot-starter</artifactId>
        <version>1.4.2</version>
    </dependency>
```

2、分布式事务上加入 @GlobalTransactional 注解

3、配置好事务分组
    seata:
      tx-service-group: tx_order_service_group

4、配置好数据源代理
    seata:
      enabled: true
      # 是否自动开启数据源代理
      enable-auto-data-source-proxy: true
      # 数据源代理模式，使用AT模式
      data-source-proxy-mode: AT

5、检查 各个微服务的 xid 是否一致

6、如果xid没有传输，可以考虑将 com.alibaba.cloud.seata.web.SeataHandlerInterceptor 加入到 spring 的拦截器链中。