package com.huan.seata.config;

import com.alibaba.cloud.seata.web.SeataHandlerInterceptor;
import com.huan.seata.interceprot.RequestLogInterceptor;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author huan.fu 2021/9/26 - 下午5:37
 */
@EnableWebMvc
public class WebMvcConfig implements WebMvcConfigurer {
    
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 添加一个请求日志输出拦截器
        registry.addInterceptor(new RequestLogInterceptor()).addPathPatterns("/**");
        // TODO 如果xid没有传输，可能需要将下档这个拦截器打开
        // registry.addInterceptor(new SeataHandlerInterceptor()).addPathPatterns("/**");
    }
}
