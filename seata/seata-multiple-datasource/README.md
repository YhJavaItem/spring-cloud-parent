# 多数据源集成

## 1、引入多数据源组件
```xml
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>dynamic-datasource-spring-boot-starter</artifactId>
    <version>3.4.1</version>
</dependency>
```

## 2、引入seata

```xml
<dependency>
    <groupId>io.seata</groupId>
    <artifactId>seata-spring-boot-starter</artifactId>
    <version>1.4.2</version>
</dependency>
```

## 3、配置文件配置多数据源

1. 此处集成seata数据源代理。  
2. 需要注册此切面的位置
3. 设置默认的数据源

```yaml
spring:
  datasource:
    dynamic:
      # 启用 seata
      seata: true
      # 模式是 at 模式
      seata-mode: at
      # 主数据源是 account 数据源
      primary: account
      # 不启用严格模式
      strict: false
      # 配置数据源切面的位置
      order: "-2147483648"
      # 每一个数据源
      datasource:
        # account 库的数据源
        account:
          url: jdbc:mysql://127.0.0.1:3306/seata_account?useUnicode=true&characterEncoding=utf8&autoReconnectForPools=true&useSSL=false
          username: root
          password: root
          driver-class-name: com.mysql.cj.jdbc.Driver
        # 订单库的数据源
        order:
          url: jdbc:mysql://127.0.0.1:3306/seata_order?useUnicode=true&characterEncoding=utf8&autoReconnectForPools=true&useSSL=false
          username: root
          password: root
          driver-class-name: com.mysql.cj.jdbc.Driver
```

## 4、关闭seata自己默认的数据源代理

```yaml
seata:
  # 是否自动开启数据源代理
  enable-auto-data-source-proxy: false
```

## 5、配置seata事物分组

```yaml
seata:
  enabled: true
  tx-service-group: tx_multiple_datasource_group # 该分组需要在seata server的配置中心中存在
```

## 6、@DS和@Transaction配置使用注意实现

1. @DS 注解要想生效，则必须代理调用方法。  
2. @DS 注解需要比 @Transaction 注解先生效。

3. 生效的例子
```java
class AccountServiceImpl{

    @Transactional(rollbackFor = Exception.class)
    @DS("account")
    public void debit(Integer id, Long amount) {
        log.info("准备扣除用户id:[{}][{}]分钱.", id, amount);
        
        LambdaUpdateWrapper<AccountDO> wrapper = Wrappers.lambdaUpdate(AccountDO.class)
                .eq(AccountDO::getId, id)
                .gt(AccountDO::getBalance, 0)
                .setSql("balance = balance -" + amount);
        
        int result = accountMapper.update(null, wrapper);
        
        if (result <= 0) {
            throw new RuntimeException("余额不足");
        }
    }
}
```

4. 不生效的例子-01

```java
import com.baomidou.dynamic.datasource.annotation.DS;class AccountServiceImpl{
    @Transactional(rollbackFor = Exception.class)
    @DS("account")
    public void debit(Integer id, Long amount) {
        log.info("准备扣除用户id:[{}][{}]分钱.", id, amount);
        
        LambdaUpdateWrapper<AccountDO> wrapper = Wrappers.lambdaUpdate(AccountDO.class)
                .eq(AccountDO::getId, id)
                .gt(AccountDO::getBalance, 0)
                .setSql("balance = balance -" + amount);
        
        int result = accountMapper.update(null, wrapper);
        
        // 此时数据源无法切换过去，需要通过代理的方式调用此方法
        切换数据源();

        if (result <= 0) {
            throw new RuntimeException("余额不足");
        }
    }
    
    @DS("aaa")
    public void 切换数据源(){
    
    }
}
```

4. 不生效的例子-02

@DS 注解写到 mapper 上， @Transaction 写到 Service 上。 此时应该切换不了，走的是默认的数据源。

## 7、本地测试的时候需要注意一下，可能application.yml配置文件没有编译到classpath环境下。