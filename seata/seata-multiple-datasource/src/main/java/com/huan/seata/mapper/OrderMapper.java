package com.huan.seata.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huan.seata.entity.OrderDO;

/**
 * @author huan.fu 2021/9/27 - 下午2:34
 */
public interface OrderMapper extends BaseMapper<OrderDO> {
    
    int createOrder(OrderDO order);
}
