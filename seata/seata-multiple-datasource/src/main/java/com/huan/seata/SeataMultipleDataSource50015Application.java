package com.huan.seata;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author huan.fu 2021/9/27 - 下午4:40
 */
@SpringBootApplication
@MapperScan(basePackages = "com.huan.seata.mapper")
public class SeataMultipleDataSource50015Application {
    public static void main(String[] args) {
        SpringApplication.run(SeataMultipleDataSource50015Application.class);
    }
}
