package com.huan.seata.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author huan.fu 2021/9/26 - 下午1:57
 */
@Getter
@Setter
@ToString
@TableName("account")
public class AccountDO {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * 姓名
     */
    private String name;
    /**
     * 余额，单位分
     */
    private Long balance;
}
