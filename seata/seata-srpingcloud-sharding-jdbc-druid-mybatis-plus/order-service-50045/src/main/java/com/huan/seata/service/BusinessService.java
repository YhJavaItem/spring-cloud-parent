package com.huan.seata.service;

/**
 * @author huan.fu 2021/9/16 - 下午2:35
 */
public interface BusinessService {
    
    void createAccountOrder(Integer accountId, Long amount, boolean hasException);
}
