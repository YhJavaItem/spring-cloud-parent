# 解决日期序列化错误

## 背景

当我们的业务表中存在 `datetime` 类型时，如果修改了这个字段的值，那么可能会报如下错误。

```
com.fasterxml.jackson.databind.exc.InvalidDefinitionException: Type id handling not implemented for type java.lang.Object (by serializer of type com.fasterxml.jackson.databind.ser.impl.UnsupportedTypeSerializer) (through reference chain: io.seata.rm.datasource.undo.BranchUndoLog["sqlUndoLogs"]->java.util.ArrayList[0]->io.seata.rm.datasource.undo.SQLUndoLog["beforeImage"]->io.seata.rm.datasource.sql.struct.TableRecords["rows"]->java.util.ArrayList[0]->io.seata.rm.datasource.sql.struct.Row["fields"]->java.util.ArrayList[2]->io.seata.rm.datasource.sql.struct.Field["value"])
```

## 解决方案

### 1、方案一

将数据库中的 `datetime` 类型修改成 `timestamp` 类型

### 2、方案二

`undo_log`默认情况下使用的是 `jackson`的序列化方式，此处我们修改成 `kryo` 的序列化方式。

1. 在seata server的配置中心中修改如下属性

```properties
client.undo.logSerialization=kryo
```

2. 项目中引入 kryo 的jar包

```xml

```

## 小知识点

### 1、查看当前undo_log的序列化方式

```java
package io.seata.rm.datasource.undo;

import io.seata.config.ConfigurationFactory;
import io.seata.core.constants.ConfigurationKeys;

import static io.seata.common.DefaultValues.DEFAULT_TRANSACTION_UNDO_LOG_SERIALIZATION;

public interface UndoLogConstants {

    String SERIALIZER_KEY = "serializer";

    // 这个地方获取 seata undo_log 的序列化方式
    String DEFAULT_SERIALIZER = ConfigurationFactory.getInstance()
        .getConfig(ConfigurationKeys.TRANSACTION_UNDO_LOG_SERIALIZATION, DEFAULT_TRANSACTION_UNDO_LOG_SERIALIZATION);

    String COMPRESSOR_TYPE_KEY = "compressorType";
}
```