package com.huan.seata.service.impl;

import com.huan.seata.service.BusinessService;
import com.huan.seata.service.OrderService;
import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @author huan.fu 2021/9/16 - 下午2:35
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class BusinessServiceImpl implements BusinessService {
    
    private final OrderService orderService;
    private final RestTemplate restTemplate;
    
    @Override
    @GlobalTransactional(rollbackFor = Exception.class)
    public void createAccountOrder(Integer accountId, Long amount, boolean hasException) {
        System.out.println("createAccountOrder:" + RootContext.getXID());
        // 1、远程扣减账户余额
        remoteDebit(accountId, amount);
        
        // 2、下订单
        orderService.createOrder(accountId, amount);
        
        if (hasException) {
            throw new RuntimeException("发生了异常，分布式事物需要会滚");
        }
    }
    
    private void remoteDebit(Integer accountId, Long amount) {
        String url = "http://localhost:50026/account/debit?id=" + accountId + "&amount=" + amount;
        String result = restTemplate.getForObject(url, String.class);
        log.info("远程扣减库存结果:[{}]", result);
    }
}
