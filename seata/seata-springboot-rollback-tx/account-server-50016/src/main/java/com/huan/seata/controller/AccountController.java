package com.huan.seata.controller;

import com.huan.seata.service.AccountService;
import io.seata.core.context.RootContext;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

/**
 * 账户控制器
 *
 * @author huan.fu 2021/9/16 - 下午1:57
 */
@RestController
@RequiredArgsConstructor
public class AccountController {
    
    private final AccountService accountService;
    
    /**
     * 返回值 如果是 <= 5，则说明这个服务有问题，需要进行分布式事物会滚
     */
    @GetMapping("account/debit")
    public Integer debit(@RequestParam("id") Integer id, @RequestParam("amount") Long amount) {
        System.out.println("debit:" + RootContext.getXID());
        accountService.debit(id, amount);
        return new Random().nextInt(10);
    }
}
