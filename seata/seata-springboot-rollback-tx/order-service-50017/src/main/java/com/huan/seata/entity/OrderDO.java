package com.huan.seata.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;
import java.util.Date;

/**
 * @author huan.fu 2021/9/16 - 下午2:33
 */
@Getter
@Setter
@ToString
public class OrderDO {
    private Integer id;
    private Integer accountId;
    private Long amount;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp orderTime;
}
