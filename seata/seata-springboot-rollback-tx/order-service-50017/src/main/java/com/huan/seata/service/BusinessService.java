package com.huan.seata.service;

/**
 * @author huan.fu 2021/9/16 - 下午2:35
 */
public interface BusinessService {
    /**
     * 手动回滚分布式事物
     */
    void rollbackTx(Integer accountId, Long amount, boolean hasException);
    
    /**
     * 挂起分布式事物
     */
    void bindAndUnBind(Integer accountId, Long amount);
}
