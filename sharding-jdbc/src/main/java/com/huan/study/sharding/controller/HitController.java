package com.huan.study.sharding.controller;

import com.huan.study.sharding.entity.Customer;
import com.huan.study.sharding.mappers.CustomerMapper;
import org.apache.shardingsphere.api.hint.HintManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author huan.fu 2021/5/27 - 上午9:08
 */
@RestController
@RequestMapping("hit")
public class HitController {

    @Resource
    private CustomerMapper customerMapper;

    /**
     * 强制走主库
     */
    @GetMapping("findCustomer")
    public Customer findCustomer() {
        try (HintManager hintManager = HintManager.getInstance()) {
            // 强制走主库
            hintManager.setMasterRouteOnly();
            // 返回数据
            return customerMapper.findCustomer("18251421051");
        }
    }
}
