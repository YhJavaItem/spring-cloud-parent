package com.huan.study.sharding.mappers;

import com.huan.study.sharding.entity.Activity;
import com.huan.study.sharding.entity.Customer;
import org.apache.ibatis.annotations.Param;

/**
 * @author huan.fu 2021/5/18 - 上午10:43
 */
public interface ActivityMapper {

    int addActivity(@Param("activityName") String activityName);

    Activity findActivity(@Param("id") Long id);
}
