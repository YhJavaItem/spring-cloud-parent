package com.huan.study.sharding.mappers;

import com.huan.study.sharding.entity.CustomerOrderItem;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author huan.fu 2021/5/24 - 下午5:29
 */
public interface CustomerOrderItemMapper {

    int createOrderItem(@Param("orderId") BigDecimal orderId, @Param("customerId") Long customerId, @Param("detail") String detail);

    CustomerOrderItem findOrderItem(@Param("orderId") BigDecimal orderId);

    List<CustomerOrderItem> findCustomerOrderItems(@Param("customerId") Long customerId);

    Map<String, Object> findOrderItemDetail(@Param("orderId") BigDecimal orderId);
}
