package com.huan.hadoop.mr;

import lombok.Getter;
import lombok.Setter;
import org.apache.hadoop.mapred.lib.db.DBWritable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * MapReduce 从数据库中读取数据，封装程这个对象，因为只需要读取数据，所以只需要实现 readFields 方法
 *
 * @author huan.fu
 * @date 2023/7/16 - 10:20
 */
@Getter
@Setter
public class Student implements DBWritable {
    /**
     * 学生编号
     */
    private long studentId;
    /**
     * 学生姓名
     */
    private String studentName;

    @Override
    public void write(PreparedStatement statement) throws SQLException {
        // ignore
    }

    @Override
    public void readFields(ResultSet rs) throws SQLException {
        this.studentId = rs.getLong(1);
        this.studentName = rs.getString(2);
    }

    @Override
    public String toString() {
        return this.studentId + "\t" + this.studentName;
    }
}
