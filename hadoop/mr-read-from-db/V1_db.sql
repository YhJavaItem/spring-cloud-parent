create table student
(
    student_id   int auto_increment comment '学生id'
        primary key,
    student_name varchar(20) null comment '学生姓名'
) comment '学生表' auto_increment = 1;

INSERT INTO student (student_id, student_name) VALUES (1, '张三');
INSERT INTO student (student_id, student_name) VALUES (2, '李四');
