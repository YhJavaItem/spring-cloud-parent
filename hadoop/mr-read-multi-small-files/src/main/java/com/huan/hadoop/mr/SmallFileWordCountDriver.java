package com.huan.hadoop.mr;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.CombineTextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * 处理多个小文件的写法，使用 CombineTextInputFormat
 *
 * @author huan.fu
 * @date 2023/7/8 - 12:26
 */
public class SmallFileWordCountDriver extends Configured implements Tool {

    public static void main(String[] args) throws Exception {
        // 构建配置对象
        Configuration configuration = new Configuration();
        // 使用 ToolRunner 提交程序
        int status = ToolRunner.run(configuration, new SmallFileWordCountDriver(), args);
        // 退出程序
        System.exit(status);
    }

    @Override
    public int run(String[] args) throws Exception {
        // 构建Job对象实例 参数（配置对象，Job对象名称）
        Job job = Job.getInstance(getConf(), "wordCont");
        // 设置mr程序运行的主类
        job.setJarByClass(SmallFileWordCountDriver.class);
        // 设置mr程序运行的 mapper类型和reduce类型
        job.setMapperClass(WordCountMapper.class);
        job.setReducerClass(WordCountReducer.class);
        // 指定mapper阶段输出的kv数据类型
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(LongWritable.class);
        // 指定reduce阶段输出的kv数据类型，业务mr程序输出的最终类型
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(LongWritable.class);

        // 设置InputFormat的实现类为CombineTextInputFormat类
        job.setInputFormatClass(CombineTextInputFormat.class);
        // 虚拟存储切片最大值设置20m
        FileInputFormat.setMaxInputSplitSize(job, 20971520);

        FileInputFormat.setInputPaths(job, new Path("/Users/huan/code/IdeaProjects/me/spring-cloud-parent/hadoop/mr-read-multi-small-files/input"));
        FileOutputFormat.setOutputPath(job, new Path("/Users/huan/code/IdeaProjects/me/spring-cloud-parent/hadoop/mr-read-multi-small-files/output"));

        // 是指 reduce task的个数（会导致输出的文件个数和reduce task的个数一样）
        job.setNumReduceTasks(1);

        return job.waitForCompletion(true) ? 0 : 1;
    }
}
