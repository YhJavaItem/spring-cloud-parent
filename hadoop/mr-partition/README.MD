# 分区知识

1. 如果ReduceTask的数量 `>` getPartition的结果数，则会多产生几个空的输出文件part-r-000xx；
2. 如果1<ReduceTask的数量 `<` getPartition的结果数，则有一部分分区数据无处安放，会Exception；
3. 如果ReduceTask的数量 `=1`，则不管MapTask端输出多少个分区文件，最终结果都交给这一个ReduceTask，最终也就只会产生一个结果文件 part-r-00000；
4. 分区号必须从零开始，逐一累加。
5. `Partitioner<K,V> map阶段输出的key和value`
6. 实现`Configurable`接口，获取`Configuration`对象

# 案例

![假设自定义分区数为5.png](假设自定义分区数为5.png)

# 默认的分区策略

hash分区策略： `org.apache.hadoop.mapred.lib.HashPartitioner`

# 需求

根据输入的数据，`不同网站`的数据`输出到不同的文件中`

# 输入数据

```text
1	13736230513	192.196.100.1	www.baidu.com	2481	24681	200
2	13846544121	192.196.100.2	www.google.com	264	0	200
3 	13956435636	192.196.100.3	www.google.com	132	1512	200
4 	13966251146	192.168.100.1	www.google.com	240	0	404
5 	18271575951	192.168.100.2	www.baidu.com	1527	2106	200
```

# 输出数据

`根据网站的不同输出到不同的网站中`

```text
5 	18271575951	192.168.100.2	www.baidu.com	1527	2106	200
1	13736230513	192.196.100.1	www.baidu.com	2481	24681	200
```

```text
4 	13966251146	192.168.100.1	www.google.com	240	0	404
3 	13956435636	192.196.100.3	www.google.com	132	1512	200
2	13846544121	192.196.100.2	www.google.com	264	0	200
```

# 实现步骤

1. 编写一个类实现`Partitioner`类，<K,V>为map阶段输出的key和value
2. `Driver`类中
```java
// 设置分区类
job.setPartitionerClass(WebSitePartitioner.class);
// 设置reduceTask的个数， reduceTask的个数需要和分区数一致。
job.setNumReduceTasks(2);
```