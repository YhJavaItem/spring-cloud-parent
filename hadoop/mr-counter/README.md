# 1、需求

使用 MapReduce 实现一个简单的 WordCount 案例， 但是如果单词中出现`java`，则认为这个这个词有问题，则需要在控制台上打印出出现的次数。  
`换句话说：` 如果我们想知道MapReduce过程中的一些信息，那么该如何实现，此处可以使用`Counter`来实现，这个Counter是MapReduce中的
全局计数器。

# 2、获取Counter

```java
// 获取一个 Counter
Counter counter=context.getCounter("自定义Counter","Java Cnt");
        // 计数器+1
        counter.increment(1);
```

# 3、日志输出

```text
[INFO ]  10:34:04,422 [main]:Counters: 31
	File System Counters
		FILE: Number of bytes read=1778
		FILE: Number of bytes written=1240161
		FILE: Number of read operations=0
		FILE: Number of large read operations=0
		FILE: Number of write operations=0
	Map-Reduce Framework
		Map input records=3
		Map output records=12
		Map output bytes=164
		Map output materialized bytes=200
		Input split bytes=149
		Combine input records=0
		Combine output records=0
		Reduce input groups=8
		Reduce shuffle bytes=200
		Reduce input records=12
		Reduce output records=8
		Spilled Records=24
		Shuffled Maps =2
		Failed Shuffles=0
		Merged Map outputs=2
		GC time elapsed (ms)=4
		Total committed heap usage (bytes)=1544552448
	Shuffle Errors
		BAD_ID=0
		CONNECTION=0
		IO_ERROR=0
		WRONG_LENGTH=0
		WRONG_MAP=0
		WRONG_REDUCE=0
	File Input Format Counters 
		Bytes Read=67
	File Output Format Counters 
		Bytes Written=83
	自定义Counter
		Java Cnt=2
```

`看最后2行的输出`