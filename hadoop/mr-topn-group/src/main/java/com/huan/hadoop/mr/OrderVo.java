package com.huan.hadoop.mr;

import lombok.Getter;
import lombok.Setter;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * 订单数据
 *
 * @author huan.fu
 * @date 2023/7/13 - 14:20
 */
@Getter
@Setter
public class OrderVo implements WritableComparable<OrderVo> {
    /**
     * 订单编号
     */
    private long orderId;
    /**
     * 订单项
     */
    private String itemId;
    /**
     * 订单项价格
     */
    private long price;

    @Override
    public int compareTo(OrderVo o) {
        // 排序： 根据 订单编号 升序， 如果订单编号相同，则根据 订单项价格 倒序
        int result = Long.compare(this.orderId, o.orderId);
        if (result == 0) {
            // 等于0说明 订单编号 相同，则需要根据 订单项价格 倒序
            result = -Long.compare(this.price, o.price);
        }
        return result;
    }

    @Override
    public void write(DataOutput out) throws IOException {
        // 序列化
        out.writeLong(orderId);
        out.writeUTF(itemId);
        out.writeLong(price);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        // 反序列化
        this.orderId = in.readLong();
        this.itemId = in.readUTF();
        this.price = in.readLong();
    }

    @Override
    public String toString() {
        return this.getOrderId() + "\t" + this.getItemId() + "\t" + this.getPrice();
    }
}
