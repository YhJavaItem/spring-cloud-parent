package com.huan.hadoop.mr;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * map 操作： 输出的key为OrderVo, 输出的value为： price
 *
 * @author huan.fu
 * @date 2023/7/13 - 14:28
 */
public class TopNMapper extends Mapper<LongWritable, Text, OrderVo, LongWritable> {

    private final OrderVo outKey = new OrderVo();
    private final LongWritable outValue = new LongWritable();

    @Override
    protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, OrderVo, LongWritable>.Context context) throws IOException, InterruptedException {
        // 获取一行数据 20230713000010  item-101    10
        String row = value.toString();
        // 根据 \t 进行分割
        String[] cells = row.split("\\s+");
        // 获取订单编号
        long orderId = Long.parseLong(cells[0]);
        // 获取订单项
        String itemId = cells[1];
        // 获取订单项价格
        long price = Long.parseLong(cells[2]);

        // 设置值
        outKey.setOrderId(orderId);
        outKey.setItemId(itemId);
        outKey.setPrice(price);
        outValue.set(price);

        // 写出
        context.write(outKey, outValue);
    }
}
