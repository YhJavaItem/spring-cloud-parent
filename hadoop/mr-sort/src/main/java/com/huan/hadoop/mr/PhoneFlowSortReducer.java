package com.huan.hadoop.mr;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * reduce 操作
 *
 * @author huan.fu
 * @date 2023/7/12 - 19:24
 */
public class PhoneFlowSortReducer extends Reducer<PhoneFlow, Text, Text, PhoneFlow> {

    @Override
    protected void reduce(PhoneFlow key, Iterable<Text> values, Reducer<PhoneFlow, Text, Text, PhoneFlow>.Context context) throws IOException, InterruptedException {
        // 输出结果
        for (Text value : values) {
            context.write(value, key);
        }
    }
}
