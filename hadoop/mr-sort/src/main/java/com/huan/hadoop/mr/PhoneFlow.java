package com.huan.hadoop.mr;

import lombok.Getter;
import lombok.Setter;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * 因为涉及到排序操作，因此需要作为Key，所以需要实现WritableComparable接口
 *
 * @author huan.fu
 * @date 2023/7/12 - 19:11
 */
@Getter
@Setter
public class PhoneFlow implements WritableComparable<PhoneFlow> {
    /**
     * 上行流量
     */
    private long upFlow;
    /**
     * 下行流量
     */
    private long downFlow;

    /**
     * 倒叙排序
     */
    @Override
    public int compareTo(PhoneFlow o) {
        return -Long.compare(this.getUpFlow(), o.getUpFlow());
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeLong(this.getUpFlow());
        out.writeLong(this.getDownFlow());
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        this.setUpFlow(in.readLong());
        this.setDownFlow(in.readLong());
    }

    @Override
    public String toString() {
        return this.getUpFlow() + "\t" + this.getDownFlow();
    }
}
