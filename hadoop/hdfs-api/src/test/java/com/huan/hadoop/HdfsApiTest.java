package com.huan.hadoop;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.junit.jupiter.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;

/**
 * hdfs api test
 * <a href="https://blog.csdn.net/fu_huo_1993/article/details/129289894">博客</a>
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class HdfsApiTest {

    private FileSystem fileSystem;

    private static final Logger log = LoggerFactory.getLogger(HdfsApiTest.class);

    @BeforeAll
    public void setUp() throws IOException, InterruptedException {
        // 1、将 HADOOP_HOME 设置到环境变量中

        Configuration configuration = new Configuration();
        // 2、此处的地址是 NameNode 的地址
        URI uri = URI.create("hdfs://192.168.121.140:8020");
        // 3、设置用户
        String user = "hadoopdeploy";

        // 此处如果不设置第三个参数，指的是客户端的身份，默认获取的是当前用户，不过当前用户不一定有权限，需要指定一个有权限的用户
        fileSystem = FileSystem.get(uri, configuration, user);
    }

    @Test
    @DisplayName("创建hdfs目录")
    void testMkdir() throws IOException {
        Path path = new Path("/bigdata/hadoop/hdfs");
        if (fileSystem.exists(path)) {
            log.info("目录 /bigdata/hadoop/hdfs 已经存在,不在创建");
            return;
        }
        boolean success = fileSystem.mkdirs(path);
        log.info("创建目录 /bigdata/hadoop/hdfs 成功:[{}?]", success);
    }

    @Test
    @DisplayName("上传文件")
    void testUploadFile() throws IOException {
        /**
         * delSrc: 文件上传后，是否删除源文件 true:删除 false:不删除
         * overwrite: 如果目标文件存在是否重写 true:重写 false:不重写
         * 第三个参数：需要上传的文件
         * 第四个参数：目标文件
         */
        fileSystem.copyFromLocalFile(false, true,
                new Path("/Users/huan/code/IdeaProjects/me/spring-cloud-parent/hadoop/hdfs-api/src/test/java/com/huan/hadoop/HdfsApiTest.java"),
                new Path("/bigdata/hadoop/hdfs"));
    }

    @Test
    @DisplayName("列出目录下有哪些文件")
    void testListFile() throws IOException {
        RemoteIterator<LocatedFileStatus> iterator = fileSystem.listFiles(new Path("/bigdata"), true);
        while (iterator.hasNext()) {
            LocatedFileStatus locatedFileStatus = iterator.next();
            Path path = locatedFileStatus.getPath();
            if (locatedFileStatus.isFile()) {
                log.info("获取到文件: {}", path.getName());
            }
        }
    }

    @Test
    @DisplayName("下载文件")
    void testDownloadFile() throws IOException {
        fileSystem.copyToLocalFile(false, new Path("/bigdata/hadoop/hdfs/HdfsApiTest.java"),
                new Path("/Users/huan/HdfsApiTest.java"), true);
    }

    @Test
    @DisplayName("删除文件")
    public void testDeleteFile() throws IOException {
        fileSystem.delete(new Path("/bigdata/hadoop/hdfs/HdfsApiTest.java"), false);
    }

    @Test
    @DisplayName("检查文件是否存在")
    public void testFileExists() throws IOException {
        Path path = new Path("/bigdata/hadoop/hdfs/HdfsApiTest.java");
        boolean exists = fileSystem.exists(path);
        log.info("/bigdata/hadoop/hdfs/HdfsApiTest.java 存在:[{}]", exists);
    }


    @AfterAll
    public void tearDown() throws IOException {
        if (null != fileSystem) {
            fileSystem.close();
        }
    }
}
