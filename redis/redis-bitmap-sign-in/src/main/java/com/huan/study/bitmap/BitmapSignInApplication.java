package com.huan.study.bitmap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author huan.fu 2021/12/13 - 下午2:54
 */
@SpringBootApplication
public class BitmapSignInApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(BitmapSignInApplication.class);
    }
}
