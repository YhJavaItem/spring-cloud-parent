package com.huan.study.redis.pubsub.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ErrorHandler;

/**
 * 处理错误
 *
 * @author huan.fu 2021/11/12 - 下午7:02
 */
@Slf4j
public class PubsubErrorHandler implements ErrorHandler {
    
    @Override
    public void handleError(Throwable t) {
        log.error("发生了错误", t);
    }
}
