package com.huan.filter.transformation;

import com.huan.filter.vo.Person;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * filter 算子： 数据过滤
 * 需求： 过滤出年龄>20的用户对象，并输出
 *
 * @author huan.fu
 * @date 2023/9/18 - 22:43
 */
public class FilterApplication {

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment environment = StreamExecutionEnvironment.getExecutionEnvironment();

        environment.fromElements(
                        new Person(1, "张三", 20),
                        new Person(2, "李四", 25),
                        new Person(3, "王五", 30)
                )
                // filter 过滤
                .filter(person -> person.getAge() > 20)
                .print();

        environment.execute("map operation");
    }
}