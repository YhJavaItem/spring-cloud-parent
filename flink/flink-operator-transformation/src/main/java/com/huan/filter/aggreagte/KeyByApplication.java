package com.huan.filter.aggreagte;

import com.huan.filter.vo.Person;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * keyBy 进行分组操作，同一个分组的数据会在同一个分区上进行处理
 *
 * @author huan.fu
 * @date 2023/9/18 - 23:02
 */
public class KeyByApplication {

    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment environment = StreamExecutionEnvironment.getExecutionEnvironment();

        environment.fromElements(
                        new Person(1, "张三", 20),
                        new Person(2, "李四", 20),
                        new Person(3, "王五", 25),
                        new Person(4, "赵六", 25),
                        new Person(5, "田七", 30)
                )
                /**
                 * keyBy 返回的是一个 KeyedStream 键控流
                 * keyBy 不是转换算子， 只是对数据进行重分组，不能设置并行度
                 * keyBy 分组与分区的关系
                 *  1） keyBy 是对数据进行分组，保证相同的key的数据在同一个分区
                 *  2） 分区： 一个子任务可以理解为一个分区，一个分区（子任务）中可以存在多个分组（key）
                 */
                .keyBy((KeySelector<Person, Integer>) Person::getAge)
                .print();

        environment.execute("map operation");
    }
}
