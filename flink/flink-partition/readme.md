# 分区

1. `shuffle` 随机分区服从均匀分布（uniform distribution），所以可以把流中的数据随机打乱，均匀地传递到下游任务分区。因为是完全随机的，所以对于同样的输入数据,
   每次执行得到的结果也不会相同。
   ![shuffle.png](shuffle.png)
2. `round robin` 轮询，简单来说就是“发牌”，按照先后顺序将数据做依次分发。通过调用DataStream的.rebalance()
   方法，就可以实现轮询重分区。rebalance使用的是Round-Robin负载均衡算法，可以将输入流数据平均分配到下游的并行任务中去。
   ![round robin.png](round robin.png)
3. `rescale` 重缩放分区和轮询分区非常相似。当调用rescale()
   方法时，其实底层也是使用Round-Robin算法进行轮询，但是只会将数据轮询发送到下游并行任务的一部分中。rescale的做法是分成小团体，发牌人只给自己团体内的所有人轮流发牌。
   ![rescale.png](rescale.png)
4. `broadcast`
   这种方式其实不应该叫做“重分区”，因为经过广播之后，数据会在不同的分区都保留一份，可能进行重复处理。可以通过调用DataStream的broadcast()
   方法，将输入数据复制并发送到下游算子的所有并行任务中去。
5. `global` 全局分区也是一种特殊的分区方式。这种做法非常极端，通过调用.global()
   方法，会将所有的输入流数据都发送到下游算子的第一个并行子任务中去。这就相当于强行让下游任务并行度变成了1，所以使用这个操作需要非常谨慎，可能对程序造成很大的压力。
6. `keyBy`  keyBy 根据某个key的值进行分区，相同的key的值进入到同一个分区中，即由同一个子任务进行处理
7. `forward` one-to-one 下游算子的并行度必须是1
8. `自定义分区` 实现 Partitioner 接口