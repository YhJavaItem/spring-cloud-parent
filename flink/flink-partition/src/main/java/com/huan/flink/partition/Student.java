package com.huan.flink.partition;

/**
 * 学生类
 *
 * @author huan.fu
 * @date 2023/9/23 - 07:23
 */
public class Student {
    /**
     * 学生id
     */
    private Long studentId;
    /**
     * 班级id
     */
    private Long classId;
    /**
     * 学生姓名
     */
    private String studentName;

    public Student() {
    }

    public Student(Long studentId, Long classId, String studentName) {
        this.studentId = studentId;
        this.classId = classId;
        this.studentName = studentName;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getClassId() {
        return classId;
    }

    public void setClassId(Long classId) {
        this.classId = classId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    @Override
    public String toString() {
        return "Student{" +
                "studentId=" + studentId +
                ", classId=" + classId +
                ", studentName='" + studentName + '\'' +
                '}';
    }
}
