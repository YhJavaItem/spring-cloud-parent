package com.huan.flink.dto;

/**
 * 鸡
 *
 * @author huan.fu
 * @date 2023/9/23 - 10:00
 */
public class Chicken extends Animal {

    public Chicken() {

    }

    public Chicken(String type, String name) {
        super(type, name);
    }

    @Override
    public String toString() {
        return "Chicken{ type=" + this.getType() + ", name=" + this.getName() + " }";
    }
}
