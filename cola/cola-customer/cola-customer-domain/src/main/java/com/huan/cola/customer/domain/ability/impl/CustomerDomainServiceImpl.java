package com.huan.cola.customer.domain.ability.impl;

import com.alibaba.cola.exception.BizException;
import com.huan.cola.customer.domain.ability.CustomerDomainService;
import com.huan.cola.customer.domain.gateway.CustomerGateway;
import com.huan.cola.customer.domain.model.Customer;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import static com.huan.cola.customer.dto.data.ErrorCode.CUSTOMER_IS_VIP_FOR_ADD;
import static com.huan.cola.customer.dto.data.ErrorCode.CUSTOMER_PHONE_EXISTS;


/**
 * @author huan.fu
 * @date 2022/6/7 - 17:08
 */
@Service
public class CustomerDomainServiceImpl implements CustomerDomainService {

    @Resource
    private CustomerGateway customerGateway;

    @Override
    public boolean checkPhoneExists(String phone) {
        return customerGateway.existsPhone(phone);
    }

    @Override
    public boolean create(Customer customer) {
        // 验证用户是否是vip客户
        if (customer.isVip()) {
            throw new BizException(CUSTOMER_IS_VIP_FOR_ADD.getErrCode(), CUSTOMER_IS_VIP_FOR_ADD.getErrDesc());
        }
        // 验证手机号是否存在
        if (checkPhoneExists(customer.getPhone())) {
            throw new BizException(CUSTOMER_PHONE_EXISTS.getErrCode(), CUSTOMER_PHONE_EXISTS.getErrDesc());
        }
        return customerGateway.create(customer) > 0;
    }
}
