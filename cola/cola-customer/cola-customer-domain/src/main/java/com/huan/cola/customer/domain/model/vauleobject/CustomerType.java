package com.huan.cola.customer.domain.model.vauleobject;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

/**
 * @author huan.fu
 * @date 2022/6/7 - 10:41
 */
@Getter
@AllArgsConstructor
public enum CustomerType {

    NORMAL(0, "普通客户"),
    VIP(1, "vip客户");

    private final Integer type;
    private final String desc;

    public static CustomerType of(Integer customerType) {
        return Arrays.stream(values())
                .filter(type -> Objects.equals(type.getType(), customerType))
                .findFirst()
                .orElse(null);
    }
}
