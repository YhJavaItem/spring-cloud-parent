package com.huan.cola.customer.domain.gateway;

import com.huan.cola.customer.domain.model.Customer;

import java.util.List;

/**
 * @author huan.fu
 * @date 2022/6/7 - 11:17
 */
public interface CustomerGateway {

    List<Customer> findAll();

    boolean existsPhone(String phone);

    int create(Customer customer);
}
