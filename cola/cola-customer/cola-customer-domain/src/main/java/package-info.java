/**
 * 领域对象： 如果某些判断，是领域对象可以自己判断的，则应该放置到领域对象中。比如：Customer，判断是否VIP客户，Customer自己知道，应该放置在Customer中
 * 领域服务： domain service 具体的业务逻辑应该放置到领域服务中。
 *
 * @author huan.fu
 * @date 2022/6/7 - 17:34
 */