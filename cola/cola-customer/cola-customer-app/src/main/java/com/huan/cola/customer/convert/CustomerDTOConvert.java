package com.huan.cola.customer.convert;

import com.huan.cola.customer.domain.model.Customer;
import com.huan.cola.customer.dto.data.CustomerDTO;

/**
 * @author huan.fu
 * @date 2022/6/7 - 15:27
 */
public class CustomerDTOConvert {

    private CustomerDTOConvert() {

    }

    public static CustomerDTO toCustomerDTO(Customer customer) {
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setId(customer.getId());
        customerDTO.setPhone(customer.getPhone());
        customerDTO.setAddress(customer.getAddress());
        customerDTO.setIdCard(customer.getIdCard());
        if (null != customer.getCustomerType()) {
            customerDTO.setCustomerType(customer.getCustomerType().getType());
            customerDTO.setCustomerTypeDesc(customer.getCustomerType().getDesc());
        }
        return customerDTO;
    }
}
