package com.huan.cola.customer.executor;

import com.alibaba.cola.dto.SingleResponse;
import com.huan.cola.customer.convertor.CustomerConvertor;
import com.huan.cola.customer.domain.ability.CustomerDomainService;
import com.huan.cola.customer.domain.model.Customer;
import com.huan.cola.customer.dto.cmd.CustomerAddCmd;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 添加客户
 *
 * @author huan.fu
 * @date 2022/6/7 - 16:14
 */
@Component
public class CustomerAddCmdExe {

    @Resource
    private CustomerDomainService customerDomainService;

    /**
     * 服务编排
     */
    public SingleResponse<Boolean> execute(CustomerAddCmd customerAddCmd) {
        Customer customer = CustomerConvertor.toDomainObject(customerAddCmd);
        // 具体的业务逻辑在领域层
        boolean result = customerDomainService.create(customer);
        return SingleResponse.of(result);
    }
}
