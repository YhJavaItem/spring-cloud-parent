package com.huan.es8.aggregations.metric;

import co.elastic.clients.elasticsearch._types.ScriptLanguage;
import co.elastic.clients.elasticsearch._types.mapping.*;
import co.elastic.clients.elasticsearch.core.SearchRequest;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.json.JsonData;
import com.huan.es8.AbstractEs8Api;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.IOException;
import java.util.Arrays;

/**
 * 平均值聚合
 * <a href="https://blog.csdn.net/fu_huo_1993/article/details/128452306">博客</a>
 * @author huan.fu
 * @date 2022/12/27 - 10:14
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AvgAggr extends AbstractEs8Api {

    // @BeforeAll
    public void createIndex() throws IOException {
        client.indices()
                .create(indexRequest ->
                        indexRequest.index("index_person")
                                .mappings(mappings ->
                                        mappings.properties("id", new Property(new LongNumberProperty.Builder().build()))
                                                .properties("name", new Property(new KeywordProperty.Builder().build()))
                                                .properties("age", new Property(new IntegerNumberProperty.Builder().build()))
                                                .properties("class", new Property(new TextProperty.Builder().fielddata(true).build()))
                                                .properties("province", new Property(new KeywordProperty.Builder().build()))
                                )
                );
        bulk("index_person", Arrays.asList(
                "{\"id\":1, \"name\":\"张三\",\"age\":18,\"class\":\"大一班\",\"province\":\"湖北\"}",
                "{\"id\":2, \"name\":\"李四\",\"age\":19,\"class\":\"大一班\",\"province\":\"湖北\"}",
                "{\"id\":3, \"name\":\"王武\",\"age\":20,\"class\":\"大二班\",\"province\":\"北京\"}",
                "{\"id\":4, \"name\":\"赵六\",\"age\":21,\"class\":\"大三班技术班\",\"province\":\"北京\"}",
                "{\"id\":5, \"name\":\"钱七\",\"age\":22,\"class\":\"大三班\",\"province\":\"湖北\"}"
        ));
    }

    @Test
    @DisplayName("平均值聚合")
    public void test01() throws IOException {
        SearchRequest request = SearchRequest.of(searchRequest ->
                searchRequest.index("index_person")
                        .size(0)
                        .aggregations("agg_01", agg ->
                                agg.avg(avg ->
                                        // 聚合的字段
                                        avg.field("age")
                                                // 如果聚合的文档缺失这个字段，则给10
                                                .missing(10)
                                )
                        )
        );
        System.out.println("request: " + request);
        SearchResponse<String> response = client.search(request, String.class);
        System.out.println("response: " + response);
    }

    @Test
    @DisplayName("脚本聚合")
    public void test02() throws IOException {
        SearchRequest request = SearchRequest.of(searchRequest ->
                searchRequest.index("index_person")
                        .size(0)
                        .aggregations("agg_01", agg ->
                                agg.avg(avg ->
                                        avg.script(script ->
                                                script.inline(inline ->
                                                        inline.lang(ScriptLanguage.Painless)
                                                                // 脚本表达式
                                                                .source("doc.age")
                                                )
                                        )
                                )
                        )
        );
        System.out.println("request: " + request);
        SearchResponse<String> response = client.search(request, String.class);
        System.out.println("response: " + response);
    }

    @Test
    @DisplayName("值脚本聚合")
    public void test03() throws IOException {
        SearchRequest request = SearchRequest.of(searchRequest ->
                searchRequest.index("index_person")
                        .size(0)
                        .aggregations("agg_01", agg ->
                                agg.avg(avg ->
                                        // 指定参与聚合的字段
                                        avg.field("age")
                                                .script(script ->
                                                        script.inline(inline ->
                                                                inline.lang(ScriptLanguage.Painless)
                                                                        // 脚本表达式
                                                                        .source("_value * params.plus")
                                                                        // 参数
                                                                        .params("plus", JsonData.of(2))
                                                        )
                                                )
                                )
                        )
        );
        System.out.println("request: " + request);
        SearchResponse<String> response = client.search(request, String.class);
        System.out.println("response: " + response);
    }

    // @AfterAll
    public void deleteIndex() throws IOException {
        deleteIndex("index_person");
    }
}
