package com.huan.es8.runtimefield;

import org.junit.jupiter.api.TestInstance;

/**
 * 对runtime field进行索引。
 *
 * 1. runtime field不会被索引，如果我们对其进行查询的话，性能会稍微低点。
 * 2. 如果某个 runtime field经常被查询，那么我们可以考虑将这个 runtime field加入到mapping中，即执行更新mapping操作。 这样那么下次索引文档(插入了一个文档)
 * 这个新的字段将会被索引，那么在查询的时候会提供更好的性能。针对该字段的查询操作我们完全不用修改。
 * 3. 在mapping阶段，如果某个字段不在我们自己设置的mapping中，但是想将这个字段映射成runtime field字段，那么可以在设置mapping时将dynamic修改成runtime
 * @author huan.fu
 * @date 2023/2/1 - 23:39
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class IndexRuntimeField extends AbstractRuntimeField{
}
