package com.huan.es8.aggregations.pipeline;

import co.elastic.clients.elasticsearch._types.mapping.*;
import co.elastic.clients.elasticsearch.core.SearchRequest;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import com.huan.es8.AbstractEs8Api;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.IOException;
import java.util.Arrays;

/**
 * 根据省分组获取省下最大的平均年龄
 *
 * @author huan.fu
 * @date 2022/12/27 - 10:14
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class 根据省分组获取省下最大的平均年龄 extends AbstractEs8Api {

    // @BeforeAll
    public void createIndex() throws IOException {
        client.indices()
                .create(indexRequest ->
                        indexRequest.index("index_person")
                                .mappings(mappings ->
                                        mappings.properties("id", new Property(new LongNumberProperty.Builder().build()))
                                                .properties("name", new Property(new KeywordProperty.Builder().build()))
                                                .properties("age", new Property(new IntegerNumberProperty.Builder().build()))
                                                .properties("class", new Property(new TextProperty.Builder().fielddata(true).build()))
                                                .properties("province", new Property(new KeywordProperty.Builder().build()))
                                )
                );
        bulk("index_person", Arrays.asList(
                "{\"id\":1, \"name\":\"张三\",\"age\":18,\"class\":\"大一班\",\"province\":\"湖北\"}",
                "{\"id\":2, \"name\":\"李四\",\"age\":19,\"class\":\"大一班\",\"province\":\"湖北\"}",
                "{\"id\":3, \"name\":\"王武\",\"age\":20,\"class\":\"大二班\",\"province\":\"北京\"}",
                "{\"id\":4, \"name\":\"赵六\",\"age\":21,\"class\":\"大三班技术班\",\"province\":\"北京\"}",
                "{\"id\":5, \"name\":\"钱七\",\"age\":22,\"class\":\"大三班\",\"province\":\"湖北\"}"
        ));
    }

    @Test
    @DisplayName("根据省分组获取省下最大的平均年龄")
    public void test01() throws IOException {
        SearchRequest request = SearchRequest.of(searchRequest ->
                searchRequest.index("index_person")
                        .size(0)
                        .aggregations("根据省分组", agg01 ->
                                agg01.terms(terms ->
                                                terms.field("province")
                                        )
                                        .aggregations("省下的平均年龄", agg02 ->
                                                agg02.avg(avg -> avg.field("age"))
                                        )
                        )
                        .aggregations("根据省分组后，平均年龄最大的省为：", agg03 ->
                                agg03.maxBucket(maxBucket ->
                                        maxBucket.bucketsPath(path -> path.single("根据省分组>省下的平均年龄"))
                                )
                        )
        );
        System.out.println("request: " + request);
        SearchResponse<String> response = client.search(request, String.class);
        System.out.println("response: " + response);
    }


    // @AfterAll
    public void deleteIndex() throws IOException {
        deleteIndex("index_person");
    }
}
