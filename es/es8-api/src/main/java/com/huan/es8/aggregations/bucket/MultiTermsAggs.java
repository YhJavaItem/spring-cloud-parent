package com.huan.es8.aggregations.bucket;

import co.elastic.clients.elasticsearch._types.InlineScript;
import co.elastic.clients.elasticsearch._types.ScriptLanguage;
import co.elastic.clients.elasticsearch._types.SortOrder;
import co.elastic.clients.elasticsearch._types.mapping.RuntimeFieldType;
import co.elastic.clients.elasticsearch.core.SearchRequest;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.util.NamedValue;
import com.huan.es8.AbstractEs8Api;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.util.Arrays;

/**
 * 多字段聚合
 *
 * @author huan.fu
 * @date 2022/11/13 - 18:26
 * @see <a href="https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-multi-terms-aggregation.html">官方文档</a>
 * @see <a href="https://blog.csdn.net/fu_huo_1993/article/details/127835403">https://blog.csdn.net/fu_huo_1993/article/details/127835403</a>
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class MultiTermsAggs extends AbstractEs8Api {

    @BeforeAll
    public void createIndex() throws IOException {
        createIndex("index_person",
                "{\n" +
                        "  \"settings\": {\n" +
                        "    \"number_of_shards\": 1\n" +
                        "  },\n" +
                        "  \"mappings\": {\n" +
                        "    \"properties\": {\n" +
                        "      \"id\": {\n" +
                        "        \"type\": \"long\"\n" +
                        "      },\n" +
                        "      \"name\": {\n" +
                        "        \"type\": \"keyword\"\n" +
                        "      },\n" +
                        "      \"province\": {\n" +
                        "        \"type\": \"keyword\",\n" +
                        "        \"copy_to\": [\"province_sex\"]\n" +
                        "      },\n" +
                        "      \"sex\": {\n" +
                        "        \"type\": \"keyword\",\n" +
                        "        \"copy_to\": [\"province_sex\"]\n" +
                        "      },\n" +
                        "      \"age\": {\n" +
                        "        \"type\": \"integer\"\n" +
                        "      },\n" +
                        "      \"address\": {\n" +
                        "        \"type\": \"text\",\n" +
                        "        \"analyzer\": \"ik_max_word\",\n" +
                        "        \"fields\": {\n" +
                        "          \"keyword\": {\n" +
                        "            \"type\": \"keyword\",\n" +
                        "            \"ignore_above\": 256\n" +
                        "          }\n" +
                        "        }\n" +
                        "      },\n" +
                        "      \"province_sex\":{\n" +
                        "        \"type\":\"keyword\"\n" +
                        "      }\n" +
                        "    }\n" +
                        "  }\n" +
                        "}");

        bulk("index_person", Arrays.asList(
                "{\"id\":1,\"name\":\"张三\",\"sex\":\"男\",\"age\":20,\"province\":\"湖北\",\"address\":\"湖北省黄冈市罗田县匡河镇\"}",
                "{\"id\":2,\"name\":\"李四\",\"sex\":\"男\",\"age\":19,\"province\":\"江苏\",\"address\":\"江苏省南京市\"}",
                "{\"id\":3,\"name\":\"王武\",\"sex\":\"女\",\"age\":25,\"province\":\"湖北\",\"address\":\"湖北省武汉市江汉区\"}",
                "{\"id\":4,\"name\":\"赵六\",\"sex\":\"女\",\"age\":30,\"province\":\"北京\",\"address\":\"北京市东城区\"}",
                "{\"id\":5,\"name\":\"钱七\",\"sex\":\"女\",\"age\":16,\"province\":\"北京\",\"address\":\"北京市西城区\"}",
                "{\"id\":6,\"name\":\"王八\",\"sex\":\"女\",\"age\":45,\"province\":\"北京\",\"address\":\"北京市朝阳区\"}"
        ));
    }

    @Test
    @DisplayName("多term聚合-根据省和性别聚合，然后根据最大年龄倒序")
    public void agg01() throws IOException {

        SearchRequest searchRequest = new SearchRequest.Builder()
                .size(0)
                .index("index_person")
                .aggregations("agg_province_sex", agg ->
                        agg.multiTerms(multiTerms ->
                                        multiTerms.terms(term -> term.field("province"))
                                                .terms(term -> term.field("sex"))
                                                .order(new NamedValue<>("max_age", SortOrder.Desc))
                                )
                                .aggregations("max_age", ageAgg ->
                                        ageAgg.max(max -> max.field("age")))

                )
                .build();
        System.out.println(searchRequest);
        SearchResponse<Object> response = client.search(searchRequest, Object.class);
        System.out.println(response);
    }

    @Test
    @DisplayName("多term聚合-根据省和性别聚合，然后根据最大年龄倒序")
    public void agg02() throws IOException {

        SearchRequest searchRequest = new SearchRequest.Builder()
                .size(0)
                .index("index_person")
                .runtimeMappings("runtime_province_sex", field -> {
                    field.type(RuntimeFieldType.Keyword);
                    field.script(script -> script.inline(new InlineScript.Builder()
                            .lang(ScriptLanguage.Painless)
                            .source("String province = doc['province'].value;\n" +
                                    "          String sex = doc['sex'].value;\n" +
                                    "          emit(province + '|' + sex);")
                            .build()));
                    return field;
                })
                .aggregations("agg_province_sex", agg ->
                        agg.terms(terms ->
                                        terms.field("runtime_province_sex")
                                                .size(10)
                                                .shardSize(25)
                                                .order(new NamedValue<>("max_age", SortOrder.Desc))
                                )
                                .aggregations("max_age", minAgg ->
                                        minAgg.max(max -> max.field("age")))
                )
                .build();
        System.out.println(searchRequest);
        SearchResponse<Object> response = client.search(searchRequest, Object.class);
        System.out.println(response);
    }

    @AfterAll
    public void deleteIndex() throws IOException {
        deleteIndex("index_person");
    }
}
