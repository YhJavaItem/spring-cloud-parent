```json
{
  "mappings": {
    // runtime 表示 对于不在这个mapping中的字段，都映射成 runtime field，runtime field不会被索引，不会出现在_source中，需要通过 fields api来获取
    "dynamic": "runtime", 
    "properties": {
      // 如果name是集合类型，也是这样写的
      "name": {
        "type": "keyword",
        // 将这个字段的值拷贝到_all字段中，但是_all字段不会被存储，这个字段也不会出现在_source中？
        "copy_to": "_all",
        // 当name时null值时的处理， 不会影响 _source 中的值
        "null_value": "--",
        // 为true表示这个字段可以被搜索,false不可以
        "index": true,
        // 开启后,text类型的字段也可以进行排序和聚合操作
        "fielddata": true
      },
      "address": {
        "type": "text",
        // 索引的分词器使用 ik_max_word
        "analyzer": "ik_max_word",
        // 搜索的分词器使用 ik_smart
        "search_analyzer": "ik_smart",
        "fields": {
          // 子字段
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "sex": {
        "type": "integer"
      },
      // 日期类型，需要注意的是保存到es中的是 零时区的值，es中保存的是秒或者毫秒，这个忘记了
      "createTime": {
        "type": "date",
        "format": [
          "yyyy-MM-dd HH:mm:ss"
        ]
      },
      // 地理位置有特殊的类型
      "location": {
        "type": "geo_point"
      },
      "addressDetail": {
        "type": "object",
        "properties": {
          "province": {
            "type": "keyword"
          }
        }
      },
      "addressNew": {
        "type": "nested",
        "properties": {
          "province": {
            "type": "keyword"
          }
        }
      },
      "_all": {
        "type": "text"
      }
    }
  }
}
```