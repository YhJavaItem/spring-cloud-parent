package com.huan.prometheus;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.binder.MeterBinder;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author huan.fu 2021/3/16 - 下午12:12
 */
@Component
public class CounterBinder implements MeterBinder {

    @Override
    public void bindTo(@NonNull MeterRegistry registry) {
        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(()->{
            Counter.builder("payment_order_cnt")
                    .baseUnit("total")
                    .description("订单的数量")
                    .tag("tag","value")
                    .register(registry)
                    .increment();
        },0,1, TimeUnit.SECONDS);
    }
}
