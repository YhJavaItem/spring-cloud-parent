# histogram的特点

1. 在Prometheus中，histogram中的桶数据是累增的。

# histogram 和 summary 的区别

1. 对于Histogram的指标，在Prometheus中我们还可以通过`histogram_quantile()`函数计算出其值的分位数，不是特别准确。
2. Sumamry的分位数则是直接在客户端计算完成，比较准确，但是相比`Histogram`更耗性能。  
3. `Summary` **无法聚合**。想象一下，prometheus抓取了一个集群下多台机器的百分位数，我们怎么根据这些数据得到整个集群的百分位数呢？
如果是P0（最小值）和P100（最大值）是可以计算，分别计算所有机器P0的最小值以及P100的最大值就行，但是其他百分位数就束手无策了。

# 代码对照图

![代码解释](https://img-blog.csdnimg.cn/510392211a174e7789cd675be0092385.png)
