package com.huan.springbatch.startjob;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.UUID;

/**
 * 启动job
 *
 * @author huan.fu
 * @date 2022/8/24 - 21:10
 */
@Component
@Slf4j
public class StartJob {

    @Autowired
    private Job job;
    @Autowired
    private JobLauncher jobLauncher;

    @PostConstruct
    public void startJob() throws JobInstanceAlreadyCompleteException, JobExecutionAlreadyRunningException, JobParametersInvalidException, JobRestartException {
        JobParameters jobParameters = new JobParametersBuilder()
                .addString("uuid", UUID.randomUUID().toString())
                .toJobParameters();
        jobLauncher.run(job, jobParameters);
        log.info("job invoked");
    }
}
