# 需求

Step1 和 Step2 是一个整体  
Step3 是另外一个整体  
Step4 是另外一个整体

Step1和Step2 与 Step3 可以并行运行

Step1和Step2和Step3 都运行完之后才可以运行 Step4

   
  线程a执行          线程b执行
____________ 并行  ___________
|  Step1   |      |  Step3  |
|  Step2   |      |         |
|__________|      |_________|
     
 上方3个Step执行完之后，执行下方Step
     |---------------|
     |     Step4     |         
     |_______________|
 
# 实现

1. 使用 Flow
2. 使用 TaskExecutor

# 参考连接

[https://docs.spring.io/spring-batch/docs/current/reference/html/index-single.html#scalabilityParallelSteps](https://docs.spring.io/spring-batch/docs/current/reference/html/index-single.html#scalabilityParallelSteps)